# See https://packaging.python.org/en/latest/distributing.html
# and https://docs.python.org/2/distutils/setupscript.html
# and https://pypi.python.org/pypi?%3Aaction=list_classifiers
from setuptools import setup, find_packages


def read_file(path):
    with open(path, "r") as f:
        contents = f.read()
        f.close()
    return str(contents)


setup(
    name='python-myninjas',
    version=read_file("VERSION.txt"),
    description=read_file("DESCRIPTION.txt"),
    long_description=read_file("README.markdown"),
    author='Shawn Davis',
    author_email='shawn@myninjas.net',
    url='https://bitbucket.org/myninjas/python-myninjas',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "bs4",
        "colorama",
        "jinja2",
        "six",
        "tabulate",
    ],
    # dependency_links=[
    #     "https://github.com/develmaycare/python-datetime-machine.git",
    # ],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    zip_safe=False,
    tests_require=[
        "bs4",
        "colorama",
        "jinja2",
        "six",
        "tabulate",
        "testfixtures",
    ],
    test_suite='runtests.runtests'
)
