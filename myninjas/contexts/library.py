"""
A context is a an object-oriented representation of a related collection of related variables.

"""
# Imports

# Exports

__all__ = (
    "Context",
)

# Classes


class Context(object):
    """Collect variables together."""

    def __init__(self, name, defaults=None):
        """Initialize the context.

        :param name: The name of the context.
        :type name: str

        :param defaults: Default values, if any.
        :type defaults: dict

        """
        self._variables = dict()
        self._name = name

        if defaults is not None:
            for key, value in defaults.items():
                self._variables.setdefault(key, value)

    def __getattr__(self, item):
        """Access variables on the context instance."""
        return self._variables.get(item)

    def __iter__(self):
        return iter(self._variables)

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self._name)

    def add(self, name, value):
        """Add a variable to the context.

        :param name: The name of the variable.
        :type name: str

        :param value: The value of the variable.

        :raise: ValueError
        :raises: ``ValueError`` if the named variable already exists.

        """
        if name in self._variables:
            raise ValueError("The %s context already has a variable named: %s" % (self._name, name))

        self._variables[name] = value

    def get(self, name, default=None):
        """Get the named variable at run time with an optional default.

        :param name: The name of the variable.
        :type name: str

        :param default: The default value if any.

        """
        if self.has(name):
            return self._variables[name]

        return default

    def get_name(self):
        """Get the name of the context.

        :rtype: str

        """
        return self._name

    def has(self, name):
        """Indicates the context has the named variable *and* that it is not ``None``.

        :param name: The name of the variable.
        :type name: str

        :rtype: bool

        """
        if name in self._variables and self._variables[name] is not None:
            return True

        return False

    def mapping(self):
        """Get the context as a dictionary.

        :rtype: dict

        """
        return self._variables

    def set(self, name, value):
        """Add or update a variable in the context.

        :param name: The name of the variable.
        :type name: str

        :param value: The value of the variable.

        .. note::
            Unlike ``add()`` or ``update()`` an exception is *not* raised whether the variable exists or not.

        """
        self._variables[name] = value

    def update(self, name, value):
        """Update an existing variable in the context.

        :param name: The name of the variable.
        :type name: str

        :param value: The value of the variable.

        :raise: KeyError
        :raises: ``KeyError`` if the named variable has not been defined.

        """
        if name not in self._variables:
            raise KeyError("The %s context does not have a variable named: %s" % (self._name, name))

        self._variables[name] = value
