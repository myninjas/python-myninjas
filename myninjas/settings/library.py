# Imports

from configparser import ConfigParser
from importlib import import_module
import os
import warnings
from ..contexts import Context
from ..utils import File, parse_jinja_template, read_file, smart_cast

# Exports

__all__ = (
    "factory",
    "BaseConfig",
    "FlatConfig",
    "INIConfig",
    "PYConfig",
    "Section",
)

# Functions


def factory(path, context=None, defaults=None, identifier=None):
    """Initialize the configuration.

    :param path: The path to the configuration file. If it does not exist, defaults may be used.
    :type path: str

    :param context: When provided, the path is parsed as a Jinja2 template.
    :type context: dict

    :param defaults: The defaults, if any, that may be used for non-existent variables.
    :type defaults: dict

    :param identifier: An optional name may be used to identify the configuration programmatically. If omitted, the
                      file the name is used (without the extension).
    :type identifier: str

    :rtype: FlatConfig | INIConfig

    :raise: RuntimeError
    :raises: A ``RuntimeError`` if the file format is not supported.

    """
    f = File(path)
    if f.extension == ".cfg":
        config_class = FlatConfig
    elif f.extension == ".ini":
        config_class = INIConfig
    elif f.extension == ".py":
        config_class = PYConfig
    # elif f.extension == ".json":
    #     config_class = JSONConfig
    # elif f.extension == ".xml":
    #     raise NotImplementedError("XML configuration is not yet supported. Bummer.")
    # elif f.extension == ".yml":
    #     config_class = YAMLConfig
    else:
        raise RuntimeError("%s is an unrecognized or unsupported configuration format: %s" % (f.extension, path))

    # Possibly use this as a basis for XML configuration:
    # commons.apache.org/proper/commons-configuration/apidocs/org/apache/commons/configuration2/XMLConfiguration.html

    return config_class(path, context=context, defaults=defaults, identifier=identifier)


# Classes


class BaseConfig(File):
    """Base class for defining configuration files."""

    def __init__(self, path, context=None, defaults=None, identifier=None):
        """Initialize the configuration.

        :param path: The path to the configuration file. If it does not exist, defaults are used.
        :type path: str

        :param identifier: An optional name may be used to identify the configuration programmatically. If omitted, the
                          file the name is used (without the extension).
        :type identifier: str

        """
        super().__init__(path)

        self.context = context
        self.defaults = defaults
        self.identifier = identifier or self.name
        self.is_loaded = False
        self._variables = dict()

        if self.defaults is not None:
            for key, value in defaults.items():
                self._variables.setdefault(key, value)

    def __getattr__(self, item):
        """Get the named variable or its default."""
        return self._variables.get(item)

    def __iter__(self):
        return iter(self._variables)

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self.identifier)

    def has(self, name):
        """Indicates whether the named variable exists *and* is not ``None``.

        :rtype: bool

        """
        if name in self._variables and self._variables[name] is not None:
            return True

        return False

    def load(self):
        """Load the configuration.

        :rtype: bool

        .. note::
            This method also sets the ``is_loaded`` attribute.

        """
        raise NotImplementedError()

    def read(self):
        """An alias of ``load()``."""
        return self.load()

    # noinspection PyMethodMayBeStatic
    def smart_cast(self, value, key=None):
        """Cast the value to the appropriate Python type during ``load()``.

        :param value: The value to be cast.

        :param key: The name of the variable.
        :type key: str

        """
        return smart_cast(value)


class FlatConfig(BaseConfig):
    """Parse a flat configuration file.

    .. code-block:: cfg

        # Comments and blank lines are ignored.
        ; Comments and blank lines are ignored.
        ssh_key = ~/.ssh/deploy
        user = deploy

    """

    def load(self):
        """Load a flat configuration file."""
        if not self.exists:
            return False

        if self.context is not None:
            lines = parse_jinja_template(self.path, self.context).split("\n")
        else:
            lines = read_file(self.path).split("\n")

        for line in lines:
            if len(line) == 0:
                continue

            if line.startswith("#") or line.startswith(";"):
                continue

            key, value = line.split("=")
            key = key.strip()
            value = self.smart_cast(value.strip(), key=key)

            self._variables[key] = value

        self.is_loaded = True

        return True


class INIConfig(BaseConfig):
    """A configuration instance."""

    def __init__(self, path, **kwargs):
        """Initialize the configuration."""
        super().__init__(path, **kwargs)

        self._sections = dict()

    def __getattr__(self, item):
        """Get the named section instance."""
        return self._sections.get(item)

    def __iter__(self):
        return iter(self._sections)

    def get_sections(self):
        """Get the sections defined in the configuration."""
        for name, instance in self._sections.items():
            yield instance

    def has(self, name, key=None):
        """Indicates whether the named section has been defined.

        :param name: The section name.
        :type name: str

        :param key: Also check if the key exists (and has a value other than ``None``).
        :type key: str

        :rtype: bool

        """
        if name not in self._sections:
            return False

        if key is not None:
            return self._sections[name].has(key)

        return True

    def load(self):
        """Load the configuration.

        :rtype: bool

        """
        if not self.exists:
            self._load_default_sections()
            return False

        ini = ConfigParser()
        if self.context is not None:
            content = parse_jinja_template(self.path, self.context)
            ini.read_string(content)
        else:
            ini.read(self.path)

        for section in ini.sections():
            kwargs = dict()
            for key, value in ini.items(section):
                kwargs[key] = self.smart_cast(value, key=key)

            self._sections[section] = Section(section, **kwargs)

        self._load_default_sections()

        self.is_loaded = True

        return True

    # noinspection PyMethodMayBeStatic
    def _get_default_sections(self):
        """Get the names of default or required configuration sections.

        :rtype: list[str]

        """
        warnings.warn("_get_default_sections() is not implemented.")
        return list()

    # noinspection PyMethodMayBeStatic
    def _get_default_values(self):
        """Get the default or required values for the default sections.

        :rtype: list[tuple(str, str, str)]

        The list returned should include a tuple for each section/value combination in the form of
        ``("section_name", "key_name", "default value")``.

        """
        warnings.warn("_get_default_values() is not implemented.")
        return list()

    def _load_default_sections(self):
        """Ensures that required sections are created with default values."""
        # Create section instances as needed.
        sections = self._get_default_sections()
        for section in sections:
            if not self.has(section):
                self._sections[section] = Section(section)

        # Populate sections with default values as needed.
        values = self._get_default_values()
        for section, key, value in values:
            if not self._sections[section].has(key):
                self._sections[section].add(key, value)


class PYConfig(BaseConfig):
    """Load configuration from a Python file."""

    def __init__(self, path, **kwargs):
        super().__init__(path, **kwargs)

        self.error = None
        self.module = None

    def __getattr__(self, item):
        """Get the named section instance."""
        return getattr(self.module, item)

    def has(self, name):
        """Indicates whether the named variable exists.

        :rtype: str

        """
        if hasattr(self.module, name) and getattr(self.module, name) is not None:
            return True

        return False

    def load(self):
        """Load configuration from a Python file."""
        if not self.exists:
            return False

        name = "%s.%s" % (self.directory.replace(os.sep, "."), self.name)
        self.module = import_module(name)
        # try:
        #     self.module = import_module(name)
        # except ImportError as e:
        #     self.error = str(e)

        self.is_loaded = True

        return True


class Section(Context):
    """An object-oriented representation of a configuration section from an INI file See :py:class:`INIConfig`."""

    def __init__(self, section_name, **kwargs):
        """Initialize the section.

        :param section_name: The section name.
        :type section_name: str

        Keyword arguments are added as context variables.

        """
        super().__init__(section_name)
        self._name = section_name

        for key, value in kwargs.items():
            self.add(key, value)
