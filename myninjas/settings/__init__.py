"""
The settings library provides classes for working with configuration data, and in particular a sane interface on top of
Python's ``configparser`` that allows INI files to be represented as objects. Additionally, sections within the INI file
are also represented as objects.

Defining a Configuration File
-----------------------------

A typical configuration file might look like so:

.. code-block:: ini

    ; etc/project.ini
    [project]
    title = Rocket Skates
    active = yes
    release = 1

    [client]
    name = ACME, Inc.
    code = ACME

Loading a Configuration File
----------------------------

An example of loading a file and working with the objects contained therein:

.. code-block:: python

    from myninjas.settings import INIConfig

    config = INIConfig("etc/project.ini")
    if config.load():
        print("Title: %s" % config.project.title)
        print("Client: %s" % config.client.name)

Loading a Configuration as a Template
-------------------------------------

It is possible to load a configuration file as a Jinja2 template prior to parsing its sections:

.. code-block:: ini

    ; project.j2.ini
    [project]
    title = {{ project_title }}
    active = yes
    release = 1

    [client]
    name = {{ client_name }}
    code = {{ client_code }}

The code above redefines the previous INI example, but changes some of the configuration into template variables.

.. important::
    The configuration loader depends upon the extension to determine the type of processing to perform. The template
    file must be named with the ``.ini`` extension and not ``.ini.j2``.

Then in your code:

.. code-block:: python

    from myninjas.settings import INIConfig

    context = {
        'client_code': "ACME",
        'client_name': "ACME, Inc."
        'project_title': "Rocket Skates",
    }

    config = INIConfig("etc/project.j2.ini", context=context)
    if config.load():
        print("Title: %s" % config.project.title)
        print("Client: %s" % config.client.name)

"""
from .library import *

__all__ = (
    "factory",
    "BaseConfig",
    "FlatConfig",
    "INIConfig",
    "PYConfig",
    "Section",
)

__version__ = "0.11.0-d"
