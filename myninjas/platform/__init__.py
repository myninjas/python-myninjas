"""
The platform library provides an object-oriented representation of the current operating system.

.. code-block:: python

    import sys

    platform = Platform(sys)

    if platform.is_linux:
        print("Linux!")
    elif platform.is_osx:
        print("OS X!")
    elif platform.is_windows:
        print("Windows!")
    else:
        print("No idea!")

"""
from .library import Platform

__all__ = (
    "Platform",
)

__version__ = "0.2.0-x"
