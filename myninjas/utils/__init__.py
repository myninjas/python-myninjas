"""
Various utilities and shortcuts that a developer might common use.

Boolean Operations
------------------

is_bool
.......

Determine if a given value is a boolean at run time.

.. code-block:: python

    from myninjas.utils import is_bool

    print(is_bool("yes"))
    print(is_bool(True))
    print(is_bool("No"))
    print(is_bool(False))

.. tip::
    By default, a liberal number of values are used to test. If you *just* want ``True`` or ``False``, simply pass
    ``(True, False)`` as ``test_values``.

to_bool
.......

Convert a given value to it's boolean equivalent.

.. code-block:: python

    from myninjas.utils import to_bool

    print(to_bool("yes"))
    print(to_bool(1))
    print(to_bool("no"))
    print(to_bool(0))

Note that an unrecognized value will raise a value error.

.. code-block:: python

    from myninjas.utils import to_bool

    value = "not a boolean"
    try:
        print(to_bool(value))
    except ValueError:
        print('"%s" is not a boolean value.' % value)

File Operations
---------------

copy_file
.........

Copy a file from one location to another.

.. code-block:: python

    from myninjas.utils import copy_file

    copy_file("readme-template.txt", "path/to/project/readme.txt")

copy_tree
.........

Recursively copy a source directory to a given destination.

.. code-block:: python

    from myninjas.utils import copy_tree

    success = copy_tree("from/path", "to/path")
    print(success)

parse_jinja_template
....................

Parse a Jinja 2 template.

.. code-block:: python

    from myninjas.utils import parse_jinja_template

    context = {
        'domain_name': "example.com",
        'first_name': "Bob",
    }

    template = "path/to/welcome.html"

    output = parse_jinja_template(template, context)


read_csv
........

Read the contents of a CSV file.

.. code-block:: text

    menu,identifier,sort_order,text,url
    main,product,10,Product,/product/
    main,solutions,20,Solutions,/solutions/
    main,resources,30,Resources,/resources/
    main,support,40,Support,https://support.example.com
    main,about,50,About,/about/
    main,contact,60,Contact,/contact/

.. code-block:: python

    from myninjas.utils import read_csv

    rows = read_csv("path/to/menus.csv", first_row_fields_names=True)
    for r in rows:
        print("%s: %s" % (row['identifier'], row['url']


read_file
.........

Read a file and return its contents.

.. code-block:: python

    from myninjas.utils import read_file

    output = read_file("path/to/readme.txt")
    print(output)


write_file
..........

Write a file.

.. code-block:: python

    from myninjas.utils import write_file

    write_file("path/to/readme.txt", "This is a test.")


Math Helpers
------------

average
.......

Calculate the average of a given number of values, taking care to handle zero division.

.. code-block:: python

    from myninjas.utils import average

    values = [1, 2, 3, 4, 5]
    print(average(values))

is_integer
..........

Indicates whether the given value is an integer. Saves a little typing.

.. code-block:: python

    from myninjas.utils import is_integer

    print(is_integer(17))
    print(is_integer(17.5))
    print(is_integer("17"))
    print(is_integer("17", cast=True))


percentage
..........

Calculate the percentage that a portion makes up of a total.

.. code-block:: python

        from myninjas.utils import percentage

        p = percentage(50, 100)
        print(p + "%")

String Operations
-----------------

base_convert
............

Convert a number between two bases of arbitrary digits.

.. code-block:: python

    from myninjas.utils import base_convert

    print(base_convert(12345))

camelcase_to_underscore
.......................

Convert a string from ``CamelCase`` to ``camel_case``.

.. code-block:: python

    from myninjas.utils import camelcase_to_underscore

    model_name = "ProjectTasks"
    print(camelcase_to_underscore(model_name))

indent
......

Indent a string.

.. code-block:: python

    from myninjas.utils import indent

    text = "This text will be indented."
    print(indent(text))

is_string
.........

Indicates whether the given value is a string. Saves a little typing.

.. code-block:: python

    from myninjas.utils import is_string

    print(is_string("testing"))
    print(is_string("17"))
    print(is_string(17))

parse_jinja_string
..................

Parse the given string as a Jinja 2 template.

.. code-block:: python

    from myninjas.utils import parse_jinja_string

    context = {
        'domain_name': "example.com",
        'first_name': "Bob",
    }

    template = "Hello {{ first_name }}, welcome to the {{ domain_name }} website!"

    output = parse_jinja_string(template, context)


strip_html_tags
...............

Strip HTML tags from a string.

.. code-block:: python

    from myninjas.utils import strip_html_tags

    html = "<p>This string contains <b>HTML</b> tags.</p>"
    print(strip_html_tags(html))

truncate
........

Get a truncated version of a string if if over the limit.

.. code-block:: python

    from myninjas.utils import truncate

    title = "This Title is Too Long to Be Displayed As Is"
    print(truncate(title))

underscore_to_camelcase
.......................

Convert a string from ``camel_case`` to ``CamelCase`` .

.. code-block:: python

    from myninjas.utils import underscore_to_camelcase

    pattern_name = "project_detail"
    print(underscore_to_camelcase(pattern_name))

underscore_to_title_case
........................

Convert a string from ``under_score_case`` to ``Title Case``.

.. code-block:: python

    from myninjas.utils import underscore_to_title_case

    pattern_name = "project_detail"
    print(underscore_to_title_case(pattern_name))

Others
------

The File Class
..............

The :py:class:`File`` class is a simple helper for working with the various attributes of a given file path.

For more robust handling of paths, see `pathlib`_.

.. _pathlib: https://docs.python.org/3/library/pathlib.html

.. code-block:: python

    from myninjas.utils import File

    f = File("/path/to/config.ini")
    print("Path: %s" % f.path)
    print("Directory: %s" % f.directory)
    print("Name: %s" % f.name
    print("Name Without Extension: %s" % f.basename)
    print("Extension: %s" % f.extension)

smart_cast
..........

Intelligently cast the given value to a Python data type.

.. code-block:: python

    from myninjas.utils import smart_cast

    value = "123"
    print(type(smart_cast(value)), smart_cast(value))

    value = "yes"
    print(type(smart_cast(value)), smart_cast(value))

"""
from .library import *

__all__ = (
    "average",
    "base_convert",
    "camelcase_to_underscore",
    "copy_file",
    "copy_tree",
    "indent",
    "is_bool",
    "is_integer",
    "is_string",
    "parse_jinja_string",
    "parse_jinja_template",
    "percentage",
    "read_csv",
    "read_file",
    "smart_cast",
    "strip_html_tags",
    "to_bool",
    "truncate",
    "underscore_to_camelcase",
    "underscore_to_title_case",
    "write_file",
    "File",
)

__version__ = "0.14.2-d"
