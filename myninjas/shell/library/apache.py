"""
Apache
......

Classes for working with commands related to the Apache web server.

"""

# Imports

from .base import Command

# Exports

__all__ = (
    "ConfigTest",
    "DisableModule",
    "DisableSite",
    "EnableModule",
    "EnableSite",
)

# Classes


class ConfigTest(Command):
    """Test the Apache configuration."""

    def __init__(self, **kwargs):
        name = "apachectl configtest"

        super().__init__(name, **kwargs)


class DisableModule(Command):
    """Disable an Apache module."""

    def __init__(self, target, **kwargs):
        name = "a2dismod %s" % target

        super().__init__(name, **kwargs)


class DisableSite(Command):
    """Disable website."""

    def __init__(self, target, **kwargs):
        name = "a2dissite %s" % target

        super().__init__(name, **kwargs)


class EnableModule(Command):
    """Enable an apache module."""

    def __init__(self, target, **kwargs):
        name = "a2enmod %s" % target

        super().__init__(name, **kwargs)


# class EnableModule(ItemizedCommand):
#     """Enable an apache module."""
#
#     def __init__(self, target, items=None, **kwargs):
#         name = "a2enmod %s" % target
#
#         super().__init__(name, items=items, **kwargs)
#
#     def get_subcommands(self, name, **kwargs):
#         """Handle itemized modules."""
#         if self.items is None:
#             self._commands.append(Command(name, **kwargs))
#             return
#
#         for item in self.items:
#             target = name.replace("$item", item)
#             self._commands.append(Command(target, **kwargs))


class EnableSite(Command):
    """Enable a website."""

    def __init__(self, target, **kwargs):
        name = "a2ensite %s" % target

        super().__init__(name, **kwargs)


MAPPING = {
    'apache.config': ConfigTest,
    'apache.configtest': ConfigTest,
    'apache.disable_mod': DisableModule,
    'apache.disable_site': DisableSite,
    'apache.enable_mod': EnableModule,
    'apache.enable_module': EnableModule,
    'apache.enable_site': EnableSite,
    'apache.test': ConfigTest,
}
