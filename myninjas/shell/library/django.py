"""
Django
......

Classes for working with commands related to Django management commands.

"""
# Imports

from .base import Command

# Exports

__all__ = (
    "Django",
)

# Classes


class Django(Command):
    """Run a Django management command (additional options are interpreted as options for the command)."""

    def __init__(self, target, **kwargs):
        _kwargs = {
            'comment': kwargs.pop("comment", None),
            'environments': kwargs.pop("environments", None),
            'path': kwargs.pop("path", None),
            'prefix': kwargs.pop("prefix", None),
            'scope': kwargs.pop("scope", None),
            'shell': kwargs.pop("shell", "/bin/bash"),
            'stop': kwargs.pop("stop", False),
            'sudo': kwargs.pop('sudo', False)
        }

        name = "./manage.py %s" % target
        for key, value in kwargs.items():
            name += " --%s=%s" % (key, value)

        super().__init__(name, **_kwargs)


# class DumpData(Django):
#
#     def __init__(self, target, **kwargs):
#         natural = kwargs.pop("natural", False)
#         super().__init__()


MAPPING = {
    'django': Django,
}


'''
# Imports
import os
from .base import Shell

# Commands


class Fixture(Shell):
    """Base class for the fixture commands."""

    def __init__(self, app_name, file_name="initial", indent=4, natural=True, output_format="json", path=None, project_name=None, **kwargs):
        """Instantiate a Django fixture.

        :param app_name: The app name.
        :type app_name: str

        :param file_name: Name of the fixture file (without the extension).
        :type file_name: str

        :param indent: Indentation to use when dumping fixture data.
        :type indent: int

        :param natural: Use natural foreign keys, or not.
        :type natural: bool

        :param output_format: Output format: "json" or "xml". Also used as the
                              extension.
        :type output_format: str

        :param path: Path of the fixture file. See notes.
        :type path: str

        :param project_name: The name of the Django project.
        :type project_name: str

        .. note::
            When path is omitted, it is automatically derived from the other
            parameters.

            Also, you may supply a path for use with string interpolation. The
            supported context variables are:

            - ``app_name``
            - ``file_name``
            - ``output_format``
            - ``project_name``.

        """
        self.app_name = app_name
        self.file_name = file_name
        self.indent = indent
        self.natural = natural
        self.output_format = output_format

        # Pass it along.
        super(Fixture, self).__init__(**kwargs)

        # Look for project_name in the context passed to the command.
        if project_name:
            self.project_name = project_name
        elif self.context.has_key("project_name"):
            self.project_name = self.context['project_name']
        else:
            self.project_name = None

        # Set the path.
        if path:
            self.path = path
        else:
            path = "%(app_name)s/fixtures/%(file_name)s.%(output_format)s"

            if self.project_name:
                path = "%(project_name)s/" + path

            self.path = path

    @property
    def directory_exists(self):
        """Determine whether the path to the fixtures file exists.

        :rtype: bool

        """
        return os.path.exists(self.dirname)

    @property
    def dirname(self):
        """Return the path to the fixtures file without the file itself.

        :rtype: str

        """
        return os.path.dirname(self._get_path())

    def _get_path(self):
        """Get the path with local context."""
        return self.path % {
            'app_name': self.app_name,
            'file_name': self.file_name,
            'output_format': self.output_format,
            'project_name': self.project_name,
        }


class DumpData(Fixture):
    """Dump fixture data."""

    def __init__(self, app_name, file_name="initial", indent=4, natural=True, output_format="json", path=None, project_name=None, **kwargs):
        super(DumpData, self).__init__(
            app_name,
            file_name=file_name,
            indent=indent,
            natural=natural,
            output_format=output_format,
            path=path,
            project_name=project_name,
            **kwargs
        )

        # Set this after calling super().
        self.template = "./manage.py dumpdata --indent=%(indent)s --format=%(output_format)s %(natural)s %(app_name)s > %(path)s"

    def _get_command(self):
        a = list()

        if not self.directory_exists:
            a.append("mkdir -p %s" % self.dirname)

        command = self.template % {
            'app_name': self.app_name,
            'output_format': self.output_format,
            'indent': self.indent,
            'natural': "--natural-foreign" if self.natural else "",
            'path': self._get_path(),
        }
        a.append(command)

        return "\n".join(a)


class LoadData(Fixture):
    """Load fixture data."""

    def __init__(self, app_name, file_name="initial", indent=4, natural=True, output_format="json", path=None, project_name=None, **kwargs):
        """Load fixture data.

        .. note::
            ``indent`` and ``natural`` are not used, but are preserved to
            maintain the API.

        """
        super(LoadData, self).__init__(
            app_name,
            file_name=file_name,
            indent=indent,
            natural=natural,
            output_format=output_format,
            path=path,
            project_name=project_name,
            **kwargs
        )

        # Set this after calling super().
        if path:
            self.template = "./manage.py loaddata %(path)s"
        else:
            self.template = "./manage.py loaddata --app %(app_name)s %(file_name)s"

    def _get_command(self):
        return self.template % {
            'app_name': self.app_name,
            'file_name': self.file_name,
            'path': self._get_path(),
        }

'''