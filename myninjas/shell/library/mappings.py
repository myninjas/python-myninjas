# Imports

from ..library.apache import MAPPING as APACHE_MAPPING
from ..library.base import MAPPING as BASE_MAPPING
from ..library.django import MAPPING as DJANGO_MAPPING
from ..library.messages import MAPPING as MESSAGES_MAPPING
from ..library.packaging import MAPPING as PACKAGING_MAPPING
from ..library.pgsql import MAPPING as PGSQL_MAPPING
from ..library.services import MAPPING as SERVICES_MAPPING
from ..library.ssl import MAPPING as SSL_MAPPING
from ..library.stackscripts import MAPPING as STACKSCRIPTS_MAPPING
from ..library.system import MAPPING as SYSTEM_MAPPING
from ..library.templates import MAPPING as TEMPLATES_MAPPING
from ..library.tarball import MAPPING as TARBALL_MAPPING
from ..library.users import MAPPING as USERS_MAPPING

# Exports

__all__ = (
    "MAPPING",
    "command_exists",
)

# Constants

MAPPING = dict()
MAPPING.update(APACHE_MAPPING)
MAPPING.update(BASE_MAPPING)
MAPPING.update(DJANGO_MAPPING)
MAPPING.update(MESSAGES_MAPPING)
MAPPING.update(PACKAGING_MAPPING)
MAPPING.update(PGSQL_MAPPING)
MAPPING.update(SERVICES_MAPPING)
MAPPING.update(SSL_MAPPING)
MAPPING.update(STACKSCRIPTS_MAPPING)
MAPPING.update(SYSTEM_MAPPING)
MAPPING.update(TEMPLATES_MAPPING)
MAPPING.update(TARBALL_MAPPING)
MAPPING.update(USERS_MAPPING)

# Functions


def command_exists(name):
    """Indicates whether the named command exists.

    :param name: The command name.
    :type name: str

    :rtype: bool

    """
    return name in MAPPING
