"""
System
......

Classes for working with commonly used system commands.

"""
# Imports

from myninjas.platform import Platform
import os
import sys
from .base import Command

platform = Platform(sys)

# Exports

__all__ = (
    "Append",
    "Copy",
    "MakeDir",
    # "MakeDirIf",
    "Permissions",
    "Remove",
    "Rsync",
    "Sed",
    "Symlink",
    "Touch",
    "VirtualEnv",
)

# Classes


class Append(Command):
    """Append to a file."""

    def __init__(self, to_path, content=None, **kwargs):
        """Append content to a file.

        :param to_path: The path to the file.
        :type to_path: str

        :param content: The content to be appended.
        :type content: str

        """
        name = 'echo "%s" >> %s' % (content, to_path)

        super().__init__(name, **kwargs)


class Copy(Command):
    """Copy a file."""

    def __init__(self, from_path, to_path, recursive=False, **kwargs):
        if recursive:
            name = "cp -R"
        else:
            name = "cp"

        name = "%s %s %s" % (name, from_path, to_path)
        super().__init__(name, **kwargs)


class MakeDir(Command):
    """Create a directory."""

    def __init__(self, directory, conditional=False, mode=None, recursive=False, **kwargs):
        tokens = list()

        if conditional:
            tokens.append('if [[ -d "%s" ]]; then' % directory)

        tokens.append("mkdir")

        # # Windows is recursive by default.
        # if "-p" in target:
        #     pass
        # else:
        #     if self.mode is not None and not platform.is_windows:
        #         tokens.append("-m %s" % self.mode)
        #
        #     if self.recursive and not platform.is_windows:
        #         tokens.append("-p")

        if mode is not None:
            tokens.append("-m %s" % mode)

        if recursive:
            tokens.append("-p")

        tokens.append(directory)

        if conditional:
            tokens.append("fi;")

        super().__init__(" ".join(tokens), **kwargs)


class Permissions(Command):
    """Set permissions on a directory or file."""

    def __init__(self, directory, group=None, mode=None, owner=None, recursive=None, **kwargs):
        commands = list()

        super().__init__("# permissions", **kwargs)

        if group is not None:
            tokens = ["chgrp"]

            if recursive:
                tokens.append("-R")

            tokens.append(group)

            tokens.append(directory)

            commands.append(Command(" ".join(tokens), **kwargs))

        if owner is not None:
            tokens = ["chown"]

            if recursive:
                tokens.append("-R")

            tokens.append(owner)

            tokens.append(directory)

            commands.append(Command(" ".join(tokens), **kwargs))

        if mode is not None:
            tokens = ["chmod"]

            if recursive:
                tokens.append("-R")

            tokens.append(str(mode))

            tokens.append(directory)

            commands.append(Command(" ".join(tokens), **kwargs))

        self.commands = commands
        # print(commands)

    def preview(self):
        a = list()
        for c in self.commands:
            a.append(c.preview())

        return "\n".join(a)

    def run(self):
        results = list()
        for c in self.commands:
            results.append(c.run())

        return all(results)


class Remove(Command):
    """Remove a file or directory."""

    def __init__(self, target, force=False, recursive=False, **kwargs):
        tokens = ["rm"]

        if force:
            tokens.append("-f")

        if recursive:
            tokens.append("-r")

        tokens.append(target)

        name = " ".join(tokens)

        super().__init__(name, **kwargs)


class Rsync(Command):
    """Use rsync to copy files and directories."""

    def __init__(self, source, target, delete=False, host=None, key_file=None, links=True, port=22, recursive=True,
                 user=None, **kwargs):

        # rsync -e "ssh -i $(SSH_KEY) -p $(SSH_PORT)" -P -rvzc --delete
        # $(OUTPUTH_PATH) $(SSH_USER)@$(SSH_HOST):$(UPLOAD_PATH) --cvs-exclude;

        if host is None:
            host = os.path.basename(source).replace("_", ".")

        if key_file is None:
            default_key_file = os.path.join("~/.ssh", os.path.basename(source))
            key_file = os.path.expanduser(default_key_file)

        if user is None:
            user = os.path.basename(source)

        tokens = list()
        tokens.append('rsync -e "ssh -i %s -p %s"' % (key_file, port))

        # --partial and --progress
        tokens.append("-P")

        tokens.append("--checksum")
        tokens.append("--compress")

        if links:
            tokens.append("--copy-links")

        if delete:
            tokens.append("--delete")

        if recursive:
            tokens.append("--recursive")

        tokens.append(source)

        tokens.append("%s@%s:%s" % (user, host, target))

        tokens.append("--cvs-exclude")

        name = " ".join(tokens)

        super().__init__(name, **kwargs)


class Sed(Command):
    """Replace text in a file."""

    def __init__(self, target, backup_extension=".b", delimiter="/", find=None, replace=None, **kwargs):
        context = {
            'backup': backup_extension,
            'delimiter': delimiter,
            'pattern': find,
            'replace': replace,
            'path': target,
        }

        template = "sed -i %(backup)s 's%(delimiter)s%(pattern)s%(delimiter)s%(replace)s%(delimiter)sg' %(path)s"

        name = template % context

        super().__init__(name, **kwargs)


class Symlink(Command):
    """Create a symlink."""

    def __init__(self, source, force=False, target=None, **kwargs):
        if target is None:
            target = os.path.basename(source)

        if force:
            name = "ln -sf %s %s" % (source, target)
        else:
            name = "ln -s %s %s" % (source, target)

        super().__init__(name, **kwargs)


class Touch(Command):
    """Touch a file or directory."""

    def __init__(self, target, **kwargs):
        name = "touch %s" % target

        super().__init__(name, **kwargs)


# class Touch(Command):
#     """Touch a file or directory."""
#
#     def __init__(self, target, items=None, **kwargs):
#
#         if items is not None:
#             self._commands = list()
#             name = "# touch multiple targets"
#
#             for i in items:
#                 _target = target.replace("$item", i)
#                 self._commands.append(Touch(_target, **kwargs))
#         else:
#             self._commands = None
#             name = "touch %s" % target
#
#         super().__init__(name, **kwargs)
#
#     def get_command(self, include_path=False):
#         if self._commands is not None:
#             a = list()
#             for c in self._commands:
#                 a.append(c.get_command(include_path=include_path))
#
#             return "\n".join(a)
#
#         return super().get_command(include_path=include_path)


class VirtualEnv(Command):
    """Create a Python virtual environment."""

    def __init__(self, name="python", **kwargs):

        super().__init__("virtualenv %s" % name, **kwargs)


MAPPING = {
    'alias': Symlink,
    'append': Append,
    'copy': Copy,
    'cp': Copy,
    'link': Symlink,
    'makedir': MakeDir,
    'mkdir': MakeDir,
    'perm': Permissions,
    'permissions': Permissions,
    'perms': Permissions,
    'remove': Remove,
    'rm': Remove,
    'rsync': Rsync,
    'sed': Sed,
    'sync': Rsync,
    'symlink': Symlink,
    'touch': Touch,
    'venv': VirtualEnv,
    'virtualenv': VirtualEnv,
}
