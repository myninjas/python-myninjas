"""
Templates
.........

Classes for creating files from pre-defined Jinja2 templates.

"""
# Imports

from jinja2 import TemplateNotFound
import logging
from myninjas.logging.constants import LOGGER_NAME as DEFAULT_LOGGER_NAME
from myninjas.utils import parse_jinja_template
import os
from .base import Command

LOGGER_NAME = os.environ.get("MYNINJAS_LOGGER_NAME", DEFAULT_LOGGER_NAME)

logger = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "Template",
)

# Classes


class Template(Command):
    """Create a file from a template."""

    def __init__(self, source, target, **kwargs):
        _kwargs = {
            'comment': kwargs.pop("comment", None),
            'environments': kwargs.pop("environments", None),
            'path': kwargs.pop("path", None),
            'prefix': kwargs.pop("prefix", None),
            'scope': kwargs.pop("scope", None),
            'shell': kwargs.pop("shell", "/bin/bash"),
            'stop': kwargs.pop("stop", False),
            'sudo': kwargs.pop('sudo', False)
        }

        self.context = kwargs.pop("context", dict())
        self.locations = kwargs.pop("locations", list())
        self.source = source
        self.target = target

        # Remaining kwargs are added to the context.
        # print(_kwargs['comment'], kwargs)
        self.context.update(kwargs)

        super().__init__("# template: %s" % source, **_kwargs)

    def get_command(self, include_path=False):

        source = self.source
        for location in self.locations:
            _source = os.path.join(location, self.source)
            if os.path.exists(_source):
                source = _source
                break

        try:
            content = parse_jinja_template(source, self.context)
        except TemplateNotFound:
            logger.error("Template not found: %s" % self.source)
            return None

        output = list()
        output.append("cat > %s << EOF" % self.target)
        output.append(content)
        output.append("EOF")

        return "\n".join(output)

    # def preview(self):
    #     command = self.get_command()
    #     if command is not None:
    #         return command.preview()
    #
    #     return ""
    #
    # def run(self):
    #     command = self.get_command()
    #     if command is not None:
    #         return command.run()
    #
    #     return False


MAPPING = {
    'template': Template,
    'tpl': Template,
}