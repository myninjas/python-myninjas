"""
StackScripts
............

Classes for Linode's StackScript feature.

"""

# Imports

from .base import Command

# Exports

__all__ = (
    "UDF",
)

# Classes


class UDF(Command):
    """Create a UDF input for a Linode StackScript.

    """
    def __init__(self, name, **kwargs):
        """Create a UDF instance.

        .. code-block:: bash

            # <UDF name="deploy_user" label="Deployment User" example="example_app" />
            # <UDF name="deploy_root" label="Deployment Root" example="/opt/example_app" />
            # <UDF name="deploy_key" label="Deploy Key" example="paste public key here" />
            # <UDF name="domain_name" label="Domain Name" example="example.com" />
            # <UDF name="postgres_version" label="Postgres Version" default="10" />

        """
        default = kwargs.pop("default", None)
        example = kwargs.pop("example", None)
        label = kwargs.pop("label", name.replace("_", " ").title())

        tokens = ['# <UDF name="%s" label="%s"' % (name, label)]

        if default is not None:
            tokens.append('default="%s"' % default)
        elif example is not None:
            tokens.append('example="%s"' % example)
        else:
            pass

        tokens.append("/>")

        command = " ".join(tokens)

        super().__init__(command, **kwargs)

    def run(self):
        """There is nothing to run."""
        print(self.get_command())
        return True


MAPPING = {
    'udf': UDF,
    'stackscript.udf': UDF,
}
