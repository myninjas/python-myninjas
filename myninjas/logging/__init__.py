"""
Python logging is powerful but can be daunting to get started. This library implements some basic formatters and a
helper class that makes logging a bit easier.

Using the Logging Helper
------------------------

The ``LoggingHelper`` may be used to easily set up basic logging to the console or to a file. Console colorization is
enabled by default (on supported platforms), but may be turned off by passing ``colorize=False`` when instantiating the
helper.

.. code-block:: python

    from myninjas.logging import LoggingHelper

    log = LoggingHelper(level=logging.DEBUG, path="tmp.log")
    logger = log.setup()

    logger.debug('This is a debug message just for you.')
    logger.info('This message is purely informational.')
    logger.warning('You better watch out!')
    logger.error('Something bad has happened.')
    logger.critical('Something even worse has happened.')

Logging Success
---------------

The logging library comes with a minor override to the logging class called :py:class:`SuccessLogger`. To use this
logger:

.. code-block:: python

    from myninjas.logging import LoggingHelper

    log = LoggingHelper(name="success-logger", success=True)
    logger = log.setup()

    logger.success("Something positive has happened.")

.. tip::
    The success logger only works for named loggers. Otherwise an ``AttributeError`` is raised: "RootLogger object has
    no attribute 'success'.

Resources
---------

- Official `Python logging documentation`_.
- A `nice logging tutorial`_.

.. _nice logging tutorial: https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/
.. _Python logging documentation: https://docs.python.org/3.7/library/logging.html

"""

from .library import *

__version__ = "0.3.1-d"
