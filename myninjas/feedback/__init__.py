"""

The feedback package provides utilities for printing feedback.

.. note::
    This is *not* a substitute or alternative to logging. See the :py:mod:`myninjas.logging` package.

The following procedural utilities are provided:

- ``colorize()``: Used by color printing functions.
- ``blue()``: Print a message in blue.
- ``green()``: Print a message in green.
- ``hr()``: Print a horizontal rule.
- ``plain()``: Print a message in plain text.
- ``red()``: Print a message in red.
- ``yellow()``: Print a message in yellow.

The :py:class:`Feedback` class may be used to collect and print messages.

.. code-block:: python

    # Initialize feedback.
    from myninjas.feedback import Feedback

    feedback = Feedback()

    # Logging feedback.
    feedback.blue("This message is informational.")
    feedback.green("Yay! It worked!")
    feedback.yellow("I think something may be wrong.")
    feedback.hr()
    feedback.red("Something is definitely wrong.")
    feedback.plain("Oh, well.")

    # Print feedback, for example, at the end of your script or at the end of a set of related statements.
    for message in feedback:
        print(message)

"""
from .library import *

__version__ = "0.7.1-a"

__all__ = (
    "BLUE",
    "GREEN",
    "RED",
    "YELLOW",
    "blue",
    "colorize",
    "green",
    "hr",
    "plain",
    "red",
    "yellow",
    "Feedback",
)
