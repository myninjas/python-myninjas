"""
The prompt library provides utilities for collecting user input on the command line.

Input Classes
-------------

The base :py:class:`Input` class forms the basis for collecting user input. It may be used on its own, but the
type-specific classes provide additional options and validation. The supported input types are:

- Boolean
- Email
- Float
- Integer
- Regex
- Secret
- String

Collecting Related Inputs
-------------------------

The :py:class:`Form` class may be used to collect related inputs together. This class is meant to be extended with
defined inputs.

.. code-block:: python

    from myninjas import prompt

    class NewUserForm(prompt.Form):
        name = prompt.String("name", minimum=2)
        email = prompt.Email("email_address", required=True)
        password1 = prompt.Secret("password", required=True)
        password2 = prompt.Secret("password_again", required=True)

    form.prompt()
    print(form.values)

Ad Hoc Usage
------------

The input classes are supported by 2 functions, which may be used on their own if desired.

A simple input looks like:

.. code-block:: python

    from myninjas.prompt import get_input

    value = get_input("Enter Your Tea Time", default="5pm")
    print(value)

Choices may also be used:

.. code-block:: python

    from myninjas.prompt import get_choice

    choices = [
        "3pm",
        "4pm",
        "5pm",
        "6pm",
    ]

    value = get_choice("Select Your Tea Time", choices=choices, default="5pm")
    print(value)

"""
from .library import *

__all__ = (
    "get_choice",
    "get_input",
    "Boolean",
    "Divider",
    "Email",
    "Float",
    "Form",
    "Input",
    "Integer",
    "Regex",
    "Secret",
    "String",
)
