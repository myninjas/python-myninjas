# Imports

from tabulate import tabulate
from .constants import OUTPUT_FORMAT_SIMPLE

# Exports

__all__ = (
    "Row",
    "Table",
)

# Classes


class Row(object):
    """A row in tabular (command line) output."""

    def __init__(self, number=None, values=None):
        """Initialize a row.

        :param number: The row number.
        :type number: int

        :param values: The values included in the row.
        :type values: list

        """
        self.number = number
        self._values = values or list()

    def __iter__(self):
        return iter(self._values)


class Table(object):
    """A table for tabular (command line) output."""

    def __init__(self, headings=None, formatting=OUTPUT_FORMAT_SIMPLE):
        """Initialize a table.

        :param headings: A list of headings.
        :type headings: list[str]

        :param formatting: The output format of  the table.
        :type formatting: str


        .. note::
            Output is generated by python-tabulate. See https://bitbucket.org/astanin/python-tabulate/

        """
        self.format = formatting
        self.headings = headings
        self._rows = list()

    def __iter__(self):
        return iter(self._rows)

    def __str__(self):
        return self.to_string()

    def add(self, values):
        """Add a row to the table.

        :param values: The values of the row.
        :type values: list

        """
        row = Row(len(self._rows) + 1, values=values)
        self._rows.append(row)

    def to_string(self):
        """Get the table as string output.

        :rtype: str

        """
        # TODO: Include the table name (if given) in table output.
        return tabulate(self._rows, headers=self.headings, tablefmt=self.format)
