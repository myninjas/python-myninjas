"""
Background
----------

`Python's context managers`_ provide support for *with* statements. This is a powerful pattern described in `PEP 343`_
which facilitates automated try/finally expressions.

This library provides somewhat common context managers that a developer may find useful.

.. _Python's context managers: https://docs.python.org/2/library/contextlib.html
.. _PEP 343: https://www.python.org/dev/peps/pep-0343/

Using Context Managers
----------------------

Capturing Output
................

Capture command line output (including errors). Especially useful for testing.

.. code-block:: python

    with captured_output() as (output, error):
        print("This is a test.")
        self.assertEqual("This is a test.", output.getValue())

Changing the Working Directory
..............................

Change the current working directory for a set of statements or commands.

.. code-block:: python

    from myninjas.context_managers import cd

    with cd("example_com/source"):
        os.system("./manage.py migrate")

Using a Virtual Environment
...........................

Activate a Python virtual environment before running a command.

.. code-block:: python

    from myninjas.context_managers import virtualenv

    with virtualenv("./manage.py migrate") as command:
        print(command)

"""
from .library import captured_output, cd, virtualenv

__all__ = (
    "captured_output",
    "cd",
    "virtualenv",
)

__version__ = "0.3.0-d"
