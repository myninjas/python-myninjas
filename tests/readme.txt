Tests are located in this directory. Tests are executed above this directory.

To run unit tests:

    python -m unittest discover;

Running tests with coverage:

    coverage run --source=. -m unittest discover;

Reviewing the coverage report:

    coverage report -m;

Reviewing the HTML coverage report:

    coverage html;
    open htmlcov/index.html;
