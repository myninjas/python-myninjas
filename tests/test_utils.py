import os
import shutil
from myninjas.utils import *
from myninjas.utils.constants import FALSE_VALUES, TRUE_VALUES
from testfixtures import LogCapture
import unittest

COUNT_2 = 2

PATH_TO_CONTENT = os.path.join("example_com", "source", "content")
PATH_TO_NOWHERE = os.path.join("path", "to", "nowhere")

# This was once used by the sites module, but was replaced by a template file. We can still use it for testing, though.
EXAMPLE_TEMPLATE = """# {{ title|default("Example Template") }}

{{ description }}

"""

# Tests


class TestFile(unittest.TestCase):

    def test_exists(self):
        f = File(os.path.join("tests", "config", "example.ini"))
        self.assertTrue(f.exists)

        f = File(os.path.join("tests", "config", "nonexistent.ext"))
        self.assertFalse(f.exists)

    def test_init(self):
        """Check that file properties are correctly initialized."""
        f = File("/path/to/config.ini")

        self.assertEqual("config.ini", f.basename)
        self.assertEqual("/path/to", f.directory)
        self.assertEqual(".ini", f.extension)
        self.assertEqual("config", f.name)
        self.assertEqual("/path/to/config.ini", f.path)

    def test_repr(self):
        f = File("path/to/config.ini")
        self.assertEqual("<File config.ini>", repr(f))


class TestUtils(unittest.TestCase):

    def setUp(self):
        self.tmp_path = os.path.join("tests", "tmp")
        if not os.path.exists(self.tmp_path):
            os.mkdir(self.tmp_path)

    def tearDown(self):
        if os.path.exists(self.tmp_path):
            shutil.rmtree(self.tmp_path, ignore_errors=True)

    def test_average(self):
        """Check that averaging works as expected."""
        a = average([1, 2, 3, 4, 5])
        self.assertEqual(3, a)
        self.assertIsInstance(a, float)

        a = average([1.1, 2.2, 3.3, 4.4, 5.5])
        self.assertEqual(3.3, a)

        a = average([])
        self.assertEqual(0.0, a)

    def test_base_convert(self):
        """Check conversion of various numbers."""

        result = base_convert(0)
        assert result == "A"

        result = base_convert(12345)
        assert result == "DNH"

        result = base_convert(-12345)
        assert result == "-DNH"

    def test_camelcase_to_underscore(self):
        """Check that CamelCase is correctly converted."""
        string = "CamelCase"
        new_string = camelcase_to_underscore(string)
        self.assertEqual("camel_case", new_string)

        string = "MultipleCamelCaseString"
        new_string = camelcase_to_underscore(string)
        self.assertEqual("multiple_camel_case_string", new_string)

        string = "not_camel_case"
        new_string = camelcase_to_underscore(string)
        self.assertEqual(string, new_string)

    def test_copy_file(self):
        """Check that files copy correctly."""
        from_path = os.path.join("tests", "readme.txt")
        to_path = os.path.join("tmp", "readme.txt")
        copy_file(from_path, to_path, make_directories=True)

        self.assertTrue(os.path.exists(to_path))

        os.remove(to_path)
        os.rmdir(os.path.dirname(to_path))

        # This will not copy because the directory doesn't exist.
        to_path = os.path.join("tmp", "readme.txt")
        success, message = copy_file(from_path, to_path)
        self.assertFalse(success)
        self.assertIsInstance(message, str)

    def test_copy_tree(self):
        """Check that a tree is copied correctly."""
        from_path = os.path.join("tests", "plugins")

        # First try a nonexistent destination.
        with LogCapture() as capture:
            success = copy_tree(from_path, os.path.join("tests", "nonexistent"))
            self.assertFalse(success)

        # Next try a good directory.
        to_path = os.path.join("tests", "tmp")
        success = copy_tree(from_path, to_path)
        self.assertTrue(success)

        # A directory with bad files.
        with LogCapture() as capture:
            from_path = os.path.join("tests", "watched")
            success = copy_tree(from_path, to_path)
            self.assertFalse(success)

    def test_indent(self):
        """Check that text indentation works."""

        self.assertEqual(indent("This text will be indented."), "    This text will be indented.")

        a = list()
        a.append("This is line 1.")
        a.append("This is line 2.")
        a.append("This is line 3.")

        text = "\n".join(a)

        index = 0
        output = indent(text)
        for line in output.splitlines():
            self.assertEqual(line, "    " + a[index])
            index += 1

    def test_is_bool(self):
        """Check that runtime boolean validation works as expected."""
        for f in FALSE_VALUES:
            self.assertTrue(is_bool(f))

        for t in TRUE_VALUES:
            self.assertTrue(is_bool(t))

        # False and 0 will result in a count of 2.
        false_count = 0
        for f in FALSE_VALUES:
            if is_bool(f, test_values=(True, False)):
                false_count += 1

        self.assertEqual(COUNT_2, false_count)

        # True and 1 will result in a count of 2.
        true_count = 0
        for t in TRUE_VALUES:
            if is_bool(t, test_values=(True, False)):
                true_count += 1

        self.assertEqual(COUNT_2, true_count)

    def test_is_integer(self):
        """Check that integer recognition works as expected."""

        self.assertTrue(is_integer(17))
        self.assertFalse(is_integer(17.5))
        self.assertFalse(is_integer("17"))
        self.assertTrue(is_integer("17", cast=True))
        self.assertFalse(is_integer("asdf", cast=True))

    def test_is_string(self):
        """Check that string recognition works as expected."""

        self.assertTrue(is_string("testing"))
        self.assertTrue(is_string("17"))

    def test_parse_jinja_string(self):
        """Check the output of string template processing."""

        # First without context.
        context = dict()
        output = parse_jinja_string(EXAMPLE_TEMPLATE, context)

        self.assertTrue("Example Template" in output)

        # Then with context.
        context = {
            'title': "Example Site",
            'description': "This is an example template.",
        }
        output = parse_jinja_string(EXAMPLE_TEMPLATE, context)
        self.assertTrue("Example Site" in output)
        self.assertTrue("This is an example template." in output)

    def test_parse_jinja_template(self):
        """Check the output of template file processing."""

        class Page(object):
            content = "<p>This is the content.</p>"

        context = {
            'page': Page(),
        }

        path = os.path.join("tests", "templates", "example.html")

        output = parse_jinja_template(path, context)
        self.assertTrue("This is the content." in output)

    def test_percentage(self):
        """Check that percentage calculations work as expected."""
        p = percentage(50, 100)
        self.assertIsInstance(p, float)
        self.assertEqual(50.0, p)
        self.assertEqual(50, p)

        p = percentage(50, 150)
        self.assertEqual(33.333333333333336, p)

        p = percentage(50, 0)
        self.assertEqual(0.0, p)

    def test_read_csv(self):
        """Check the output of reading a CSV file."""
        path = os.path.join("tests", "data", "example.csv")

        # Without DictReader.
        rows = read_csv(path)
        count_4 = 4
        self.assertEqual(count_4, len(rows))

        # With DictReader.
        rows = read_csv(path, first_row_field_names=True)
        count_3 = 3
        self.assertEqual(count_3, len(rows))

    def test_read_file(self):
        """Check that a file may be read."""
        path = os.path.join("tests", "readme.txt")
        output = read_file(path)
        self.assertTrue("Tests are located in this directory." in output)

    def test_smart_cast(self):
        """Check that values are correctly cast to a Python data type."""
        value = "123"
        self.assertIsInstance(smart_cast(value), int)

        value = "yes"
        self.assertIsInstance(smart_cast(value), bool)

        value = "why?"
        self.assertIsInstance(smart_cast(value), str)

    def test_strip_html_tags(self):
        """Check that HTML is removed from a string."""

        html = "<p>This string contains <b>HTML</b> tags.</p>"
        plain = strip_html_tags(html)
        self.assertEqual("This string contains HTML tags.", plain)

    def test_truncate(self):
        """Check the output of string truncation."""
        # It should be safe to submit an empty string.
        output = truncate(None)
        self.assertTrue(len(output) == 0)

        # A string is mirrored if it doesn't exceed the limit.
        string = "1234567890"
        output = truncate(string, limit=10)
        self.assertEqual(string, output)

        # Continuation impacts the limit.
        string = "1234567890"
        limit = 9
        output = truncate(string, limit=limit)
        self.assertEqual(len(output), limit)
        self.assertEqual("123456...", output)

        # Check without continuation.
        string = "1234567890"
        limit = 5
        output = truncate(string, continuation=None, limit=limit)
        self.assertEqual(len(output), limit)
        self.assertEqual("12345", output)

    def test_to_bool(self):
        """Check that boolean conversion works as expected."""
        for f in FALSE_VALUES:
            self.assertFalse(to_bool(f))

        for t in TRUE_VALUES:
            self.assertTrue(to_bool(t))

        self.assertRaises(ValueError, to_bool, "asdf")
        self.assertRaises(ValueError, to_bool, 1.1)

    def test_underscore_to_camelcase(self):
        """Check that underscores are correctly converted."""
        string = "camel_case"
        new_string = underscore_to_camelcase(string)
        self.assertEqual("CamelCase", new_string)

        string = "multiple_camel_case_string"
        new_string = underscore_to_camelcase(string)
        self.assertEqual("MultipleCamelCaseString", new_string)

        string = "nounderscore"
        new_string = underscore_to_camelcase(string)
        self.assertEqual("Nounderscore", new_string)

    def test_underscore_to_title_case(self):
        """Check that underscores are correctly converted."""
        string = "title_case"
        new_string = underscore_to_title_case(string)
        self.assertEqual("Title Case", new_string)

        string = "multiple_title_case_string"
        new_string = underscore_to_title_case(string)
        self.assertEqual("Multiple Title Case String", new_string)

        string = "nounderscore"
        new_string = underscore_to_title_case(string)
        self.assertEqual("Nounderscore", new_string)

    def test_write_file(self):
        """Check that files are written."""
        path = os.path.join("tmp", "readme.txt")
        content = u"This is a test file."

        write_file(path, content, make_directories=True)

        self.assertTrue(os.path.exists(path))

        os.remove(path)
        os.rmdir(os.path.dirname(path))
