from myninjas.context_managers import captured_output
from myninjas.shell import abort, command_exists, Command, ItemizedCommand
from myninjas.shell.constants import EXIT_SUCCESS, EXIT_UNKNOWN
from myninjas.shell.library.apache import *
from myninjas.shell.library.django import Django
from myninjas.shell.library.messages import Message, Slack
from myninjas.shell.library.packaging import Apt, Pip, Yum
from myninjas.shell.library.pgsql import *
from myninjas.shell.library.services import *
from myninjas.shell.library.ssl import *
from myninjas.shell.library.stackscripts import *
from myninjas.shell.library.system import *
from myninjas.shell.library.tarball import *
from myninjas.shell.library.templates import *
from myninjas.shell.library.users import *
from myninjas.shell.scripts import factory as command_factory, Script
import os
import unittest

# Tests


class TestApache(unittest.TestCase):

    def test_config_test(self):
        c = ConfigTest()
        self.assertEqual("apachectl configtest", c.preview())

    def test_disable_module(self):
        c = DisableModule("ssl")
        self.assertEqual("a2dismod ssl", c.preview())

    def test_disable_site(self):
        c = DisableSite("000-default.conf")
        self.assertEqual("a2dissite 000-default.conf", c.preview())

    def test_enable_module(self):
        c = EnableModule("rewrite")
        self.assertEqual("a2enmod rewrite", c.preview())

        args = ["$item"]
        kwargs = {
            'items': ["rewrite", "ssl", "wsgi"],
        }
        c = command_factory("apache.enable_module", *args, **kwargs)
        # c = EnableModule("$item", items=items)
        p = c.preview()
        self.assertTrue("rewrite" in p)
        self.assertTrue("ssl" in p)
        self.assertTrue("wsgi" in p)

    def test_enable_site(self):
        c = EnableSite("example.com.conf")
        self.assertEqual("a2ensite example.com.conf", c.preview())


class TestCommand(unittest.TestCase):

    def test_abort(self):
        """Check that abort works as expected."""
        try:
            abort("")
        except SystemExit:
            self.assertTrue(True)

        # noinspection PyUnusedLocal
        with captured_output() as (output, error):
            try:
                abort("I can't go on.")
            except SystemExit:
                self.assertTrue(True)

    def test_preview(self):
        """Test the preview output of a command."""
        command = Command("ls -ls")
        self.assertEqual("ls -ls", command.preview())

        command = Command("ls -ls", path="myninjas")
        self.assertEqual("(cd myninjas && ls -ls)",command.preview())

        command = Command("ls -ls", prefix='echo "testing"')
        self.assertEqual('echo "testing" && ls -ls', command.preview())

        command = Command("ls -ls", path="myninjas", prefix='echo "testing"')
        self.assertEqual('(cd myninjas && echo "testing" && ls -ls)', command.preview())

    def test_repr(self):
        """Test the representation of a command."""
        command = Command("ls -ls")
        self.assertEqual("<Command: ls -ls>", repr(command))

    def test_run(self):
        """Test running a command."""
        command = Command("ls -ls", path="myninjas", prefix='echo "testing"')
        command.run()
        self.assertEqual(EXIT_SUCCESS, command.code)

        command = Command("ls -ls", path="nonexistent")
        command.run()
        self.assertEqual(EXIT_UNKNOWN, command.code)

    def test_string(self):
        """Test the string output of a command."""
        command = Command("ls -ls")
        self.assertEqual("ls -ls", str(command))

    def test_sudo(self):
        command = Command("ls -ls", sudo=True)
        self.assertEqual("sudo ls -ls", command.preview())

        command = Command("ls -ls", sudo="bob")
        self.assertEqual("sudo -u bob ls -ls", command.preview())


class TestItemizedCommand(unittest.TestCase):

    def test_init(self):
        args = ["ls -ls $item"]
        items = ["/opt", "/tmp", "/usr"]
        c = ItemizedCommand(Command, items, *args, sudo="bob")
        self.assertEqual(3, len(c.get_commands()))

    def test_repr(self):
        args = ["/path/to/$item"]
        items = ["www", "www/_assets", "www/_content"]
        c = ItemizedCommand(MakeDir, items, *args)
        self.assertEqual("<ItemizedCommand: MakeDir>", repr(c))

    def test_run(self):
        args = ["ls -ls $item"]
        items = ["/opt", "/tmp", "/usr"]
        c = ItemizedCommand(Command, items, *args)

        self.assertTrue(c.run())

    def test_run_fail(self):
        args = ["ls -ls $item"]
        items = ["/opt", "/tmp", "/nonexistent"]
        c = ItemizedCommand(Command, items, *args)

        self.assertFalse(c.run())

    def test_str(self):
        args = ["/path/to/$item"]
        kwargs = {
            'conditional': False,
        }
        items = ["www"]
        c = ItemizedCommand(MakeDir, items, *args, **kwargs)
        print(c.preview())
        self.assertEqual("mkdir /path/to/www", str(c))


class TestDjango(unittest.TestCase):

    def test_manage(self):
        c = Django("collectstatic", path="/path/to/example_com", prefix="source ../python/bin/activate")
        o = "(cd /path/to/example_com && source ../python/bin/activate && ./manage.py collectstatic)"
        self.assertEqual(o, c.preview())

    def test_settings(self):
        c = Django("migrate", path="/path/to/example_com", prefix="source ../python/bin/activate",
                   settings="tenants.acme_example_app.settings")
        o = "(cd /path/to/example_com && source ../python/bin/activate && ./manage.py migrate --settings=tenants.acme_example_app.settings)"
        self.assertEqual(o, c.preview())


class TestMapping(unittest.TestCase):

    def test_command_exists(self):
        self.assertTrue(command_exists("run"))
        self.assertFalse(command_exists("nonexistent"))


class TestMessages(unittest.TestCase):

    def test_message(self):
        c = Message("This is a test.")
        o = 'echo "This is a test."'
        self.assertEqual(o, c.preview())

    def test_slack(self):
        c = Slack("This is a test.", "asdf1234")
        self.assertEqual("https://slack.com/url/to/be/determined", c.preview())


class TestPackaging(unittest.TestCase):

    def test_apt(self):
        c = Apt("apache2")
        self.assertEqual("apt-get install -y apache2", c.preview())

    def test_pip(self):
        c = Pip("django")
        self.assertEqual("pip3 install django", c.preview())

    def test_yum(self):
        c = Yum("php7")
        self.assertEqual("yum -y install php7", c.preview())


class TestPostgres(unittest.TestCase):

    def test_create_database(self):
        c = CreateDatabase("example_app", admin_pass="testpass", owner="example_app")
        o = 'export PGPASSWORD="testpass" && createdb -h localhost -U postgres -O example_app example_app'
        self.assertEqual(o, c.preview())

        c = CreateDatabase("example_app", owner="example_app")
        o = "createdb -h localhost -U postgres -O example_app example_app"
        self.assertEqual(o, c.preview())

    def test_create_user(self):
        c = CreateUser("example_app", admin_pass="testpass", password="testpass")
        o = "export PGPASSWORD=\"testpass\" && createuser -h localhost -U postgres -DRS example_app && psql -h localhost -U postgres -c \"ALTER USER example_app WITH ENCRYPTED PASSWORD 'testpass';\""
        self.assertEqual(o, c.preview())

    def test_database_exists(self):
        c = DatabaseExists("example_app")
        o = "psql -h localhost -U postgres -lqt | cut -d \| -f 1 | grep -qw example_app"
        self.assertEqual(o, c.preview())

    def test_drop_database(self):
        c = DropDatabase("example_app", admin_pass="testpass")
        o = 'export PGPASSWORD="testpass" && dropdb -h localhost -U postgres example_app'
        self.assertEqual(o, c.preview())

    def test_drop_user(self):
        c = DropUser("example_app", admin_pass="testpass")
        o = "export PGPASSWORD=\"testpass\" && dropuser -h localhost -U postgres example_app"
        self.assertEqual(o, c.preview())

    def test_dump_database(self):
        c = DumpDatabase("example_app", admin_pass="testpass")
        o = 'export PGPASSWORD="testpass" && pg_dump -h localhost -U postgres example_app.sql example_app'
        self.assertEqual(o, c.preview())

    def test_psql(self):
        c = PSQL("SELECT * FROM users;", database="example_app", password="testpass", user="example_apps")
        o = 'export PGPASSWORD="testpass" && psql -h localhost -U example_apps -d example_app -c "SELECT * FROM users;"'
        self.assertEqual(o, c.preview())


class TestScript(unittest.TestCase):

    def test_add(self):
        s = Script("testing")
        s.add("ls -ls")
        self.assertEqual(1, len(s._commands))

        s.add(Command("du -hs"))
        self.assertEqual(2, len(s._commands))

        try:
            s.add(123, comment="Raises a type error.")
        except TypeError:
            self.assertTrue(True)

    def test_exists(self):
        s = Script("testing")
        s.path = os.path.join("tests", "config", "steps.cfg")
        self.assertTrue(s.exists)

        s.path = os.path.join("path", "to", "nonexistent", "steps.cfg")
        self.assertFalse(s.exists)

    def test_get_commands(self):
        s = Script("testing")
        s.add("ls -ls")
        s.add(Command("du -hs"))

        commands = s.get_commands()
        self.assertIsInstance(commands, list)
        self.assertEqual(2, len(commands))

    def test_get_template_locations(self):
        s = Script("testing")
        s.path = os.path.join("tests", "config", "steps.cfg")

        locations = s.get_template_locations()
        self.assertTrue("tests/config/templates" in locations[0])

    def test_iter(self):
        s = Script("testing")
        s.add("ls -ls")
        s.add(Command("du -hs"))

        for c in s:
            self.assertIsInstance(c, Command)

    def test_load(self):
        class Page(object):
            content = "This is a test."

        context = {
            'page': Page(),
        }

        s = Script("testing")
        s.load(os.path.join("tests", "config", "steps.cfg"), context=context)
        self.assertEqual(7, len(s._commands))
        # print(s.to_string())

        s = Script("nonexistent")
        s.load(os.path.join("path", "to", "nonexistent", "steps.cfg"))
        self.assertFalse(s.is_loaded)

        context = {
            'domain_tld': "example_com",
            'release_root': "/var/www/example_com",
        }
        s = Script("with-context")
        s.load(os.path.join("tests", "config", "steps.cfg.j2"), context=context)
        self.assertEqual(5, len(s._commands))
        self.assertTrue("example_com" in s.to_string())
        self.assertTrue("/var/www/example_com" in s.to_string())
        # print(s.to_string())

    def test_repr(self):
        s = Script("testing")
        self.assertEqual("<Script testing>", repr(s))

    def test_str(self):
        s = Script("testing")
        s.add(Command("ls -ls", comment="This is a test."))
        self.assertTrue("This is a test." in str(s))

    def test_to_string(self):
        s = Script("testing")
        s.add(Command("ls -ls", comment="This is a test."))
        self.assertTrue("This is a test." in s.to_string())


class TestServices(unittest.TestCase):

    def test_reload(self):
        c = Reload("apache2")
        o = "service apache2 reload"
        self.assertEqual(o, c.preview())

    def test_restart(self):
        c = Restart("apache2")
        o = "service apache2 restart"
        self.assertEqual(o, c.preview())

    def test_start(self):
        c = Start("apache2")
        o = "service apache2 start"
        self.assertEqual(o, c.preview())

    def test_stop(self):
        c = Stop("apache2")
        o = "service apache2 stop"
        self.assertEqual(o, c.preview())


class TestSSL(unittest.TestCase):

    def test_certbot(self):
        c = Certbot("example.com", email="webmaster@example.com", webroot="/path/to/www")
        o = "certbot certonly --agree-tos --email webmaster@example.com -n --webroot -w /path/to/www -d example.com"
        self.assertEqual(o, c.preview())


class TestStackScripts(unittest.TestCase):

    def test_udf(self):
        c = UDF("deploy_user", default="deploy")
        self.assertEqual('# <UDF name="deploy_user" label="Deploy User" default="deploy" />', c.preview())

        c = UDF("deploy_user", example="deploy")
        self.assertEqual('# <UDF name="deploy_user" label="Deploy User" example="deploy" />', c.preview())

        # noinspection PyUnusedLocal
        with captured_output() as (output, error):
            result = c.run()
            self.assertTrue(result)
            self.assertTrue(len(output.getvalue()) > 0)


class TestSystem(unittest.TestCase):

    def test_append(self):
        c = Append("/path/to/file.txt", content="This is a test.")
        o = 'echo "This is a test." >> /path/to/file.txt'
        self.assertEqual(o, c.preview())

    def test_copy(self):
        c = Copy("/path/to/dir/", "/path/to/other/", recursive=True)
        o = "cp -R /path/to/dir/ /path/to/other/"
        self.assertEqual(o, c.preview())

        c = Copy("/path/to/file.txt", "/path/to/other.txt")
        o = "cp /path/to/file.txt /path/to/other.txt"
        self.assertEqual(o, c.preview())

    def test_makedir(self):
        c = MakeDir("/path/to/dir", mode="755", recursive=True)
        o = "mkdir -m 755 -p /path/to/dir"
        self.assertEqual(o, c.preview())

        c = MakeDir("/path/to/dir", conditional=True, mode="755", recursive=True)
        o = 'if [[ -d "/path/to/dir" ]]; then mkdir -m 755 -p /path/to/dir fi;'
        self.assertEqual(o, c.preview())

    def test_permissions(self):
        c = Permissions("/path/to/dir", group="www-data", mode="755", owner="bob", recursive=True)

        self.assertEqual(3, len(c.commands))

        p = c.preview()
        self.assertTrue("chgrp" in p)
        self.assertTrue("chown" in p)
        self.assertTrue("chmod" in p)

        c = Permissions(os.path.join("tests", "data", "example.csv"), mode=775)
        result = c.run()
        self.assertTrue(result)

    def test_remove(self):
        c = Remove("/path/to/file.txt", force=True, recursive=True)
        o = "rm -f -r /path/to/file.txt"
        self.assertEqual(o, c.preview())

    def test_rysnc(self):
        c = Rsync(
            "/path/to/source",
            "/path/to/remote/",
            delete=True,
            host="127.0.0.1",
            key_file="~/.ssh/example",
            links=True,
            recursive=True,
            user="bob",
        )
        o = 'rsync -e "ssh -i ~/.ssh/example -p 22" -P --checksum --compress --copy-links --delete --recursive /path/to/source bob@127.0.0.1:/path/to/remote/ --cvs-exclude'
        self.assertEqual(o, c.preview())

        c = Rsync(
            "/path/to/example_app",
            "/path/to/remote/",
            delete=True,
            links=True,
            recursive=True,
        )
        o = 'rsync -e "ssh -i /Users/shawn/.ssh/example_app -p 22" -P --checksum --compress --copy-links --delete --recursive /path/to/example_app example_app@example.app:/path/to/remote/ --cvs-exclude'
        self.assertEqual(o, c.preview())

    def test_sed(self):
        c = Sed("/path/to/file.txt", find="foo", replace="bar")
        o = "sed -i .b 's/foo/bar/g' /path/to/file.txt"
        self.assertEqual(o, c.preview())

    def test_symlink(self):
        c = Symlink("/path/to/file.txt", force=True)
        o = "ln -sf /path/to/file.txt file.txt"
        self.assertEqual(o, c.preview())

        c = Symlink("/path/to/file.txt")
        o = "ln -s /path/to/file.txt file.txt"
        self.assertEqual(o, c.preview())

    def test_touch(self):
        c = Touch("/path/to/file.txt")
        self.assertEqual("touch /path/to/file.txt", c.preview())

        args = ["/path/to/$item"]
        kwargs = {
            'items': ["1.txt", "2.txt", "3.txt"],
        }
        c = command_factory("touch", *args, **kwargs)
        self.assertTrue(3, len(c.get_commands()))
        output = c.get_command()
        self.assertTrue("1.txt" in output)
        self.assertTrue("2.txt" in output)
        self.assertTrue("3.txt" in output)

    def test_virtualenv(self):
        c = VirtualEnv()
        self.assertEqual("virtualenv python", c.preview())


class TestTarball(unittest.TestCase):

    def test_archive(self):
        c = Archive(
            "/path/to/example_app",
            absolute=True,
            exclude="*.tmp",
            strip=1,
            view=True
        )
        o = "tar -czPv --exclude *.tmp --strip-components 1 -f ./archive.tgz /path/to/example_app"
        self.assertEqual(o, c.preview())

    def test_extract(self):
        c = Extract(
            "/path/to/archive.tgz",
            absolute=True,
            exclude="*.log",
            strip=1,
            view=True
        )
        o = "tar -xzPv --exclude *.log --strip-components 1 -f /path/to/archive.tgz ."
        self.assertEqual(o, c.preview())


class TestTemplate(unittest.TestCase):

    def test_get_command(self):
        class Page(object):
            content = "This is a test."

        context = {
            'page': Page(),
        }

        locations = [
            os.path.join("tests", "templates")
        ]
        c = Template("example.html", "/path/to/output.html", context=context, locations=locations)
        output = c.get_command()
        self.assertIsNotNone(output)
        self.assertTrue("This is a test." in output)

        c = Template("nonexistent-template.html", "/path/to/output.html")
        output = c.get_command()
        self.assertIsNone(output)


class TestUsers(unittest.TestCase):

    def test_init(self):
        c = AddUser("deploy")
        o = 'adduser deploy --disable-password --gecos ""'
        self.assertEqual(o, c.preview())

        c = AddUser("deploy", home="/var/www/example_app")
        o = 'adduser deploy --disable-password --gecos "" --home /var/www/example_app'
        self.assertEqual(o, c.preview())

    def test_get_command(self):
        c = AddUser("deploy", groups=["admin", "deployers"])
        o = c.get_command()
        self.assertTrue("adduser deploy admin" in o)
        self.assertTrue("adduser deploy deployers" in o)

        c = AddUser("deploy", groups=["sudo"])
        o = c.get_command()
        self.assertTrue('echo "deploy ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/deploy' in o)