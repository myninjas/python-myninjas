import os
from myninjas.context_managers import captured_output, cd, virtualenv
from myninjas.shell import Command
import unittest
# noinspection PyCompatibility
from unittest.mock import patch


class TestContextManagers(unittest.TestCase):

    def test_captured_output(self):
        """Check that captured output works as expected."""
        with patch('builtins.input', return_value='yes'):
            with captured_output() as (output, error):
                result = input("This is a Test (yes/no)? ")
                self.assertEqual("yes", result)

    def test_cd(self):
        """Check that cd works as expected."""
        path = os.path.join("tests", "data")
        with cd(path):
            command = Command("ls -ls")
            command.run()
            self.assertTrue("example.csv" in command.output)

    def test_virtualenv(self):
        """Check that virtualenv works as expected."""
        with virtualenv("./manage.py migrate") as command:
            self.assertEqual("source python/bin/activate && ./manage.py migrate", command)
