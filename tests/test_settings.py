from myninjas.settings import factory, BaseConfig, FlatConfig, INIConfig, PYConfig, Section
import os
import unittest

EXAMPLE_CFG = os.path.join("tests", "config", "example.cfg")
EXAMPLE_CFG_TEMPLATE = os.path.join("tests", "config", "example.j2.cfg")
EXAMPLE_INI = os.path.join("tests", "config", "example.ini")
EXAMPLE_INI_TEMPLATE = os.path.join("tests", "config", "example.j2.ini")
EXAMPLE_PY = os.path.join("tests", "config", "config_test.py")
# EXAMPLE_PY_BAD = os.path.join("tests", "config", "config_test_bad.py")

# Tests


class TestBaseConfig(unittest.TestCase):

    def test_getattr(self):
        class DummyConfig(BaseConfig):
            def load(self):
                self._variables["test1"] = True

        c = DummyConfig("nonexistent.txt")
        c.load()
        self.assertEqual(True, c.test1)

    def test_has(self):
        class DummyConfig(BaseConfig):
            def load(self):
                self._variables["test1"] = True
                self._variables["test2"] = None

        c = DummyConfig("nonexistent.txt")
        c.load()

        self.assertTrue(c.has("test1"))
        self.assertFalse(c.has("test2"))

    def test_init(self):
        class DummyConfig(BaseConfig):
            def load(self):
                self._variables["test1"] = True

        defaults = {
            'test1': False,
            'test2': 123,
            'test3': "def",
        }
        c = DummyConfig("nonexistent.txt", defaults=defaults)
        c.load()
        self.assertTrue(len(c._variables), 3)

        c.load()
        self.assertEqual(c.test1, True)
        self.assertEqual(c.test2, 123)
        self.assertEqual(c.test3, "def")

    def test_iter(self):
        class DummyConfig(BaseConfig):
            def load(self):
                self._variables["test1"] = True
                self._variables["test2"] = True
                self._variables["test3"] = True

        c = DummyConfig("nonexistent.txt")
        c.load()
        for i in c:
            self.assertTrue(i)

    def test_load(self):
        c = BaseConfig("nonexistent.txt")
        self.assertRaises(NotImplementedError, c.load)

    def test_read(self):
        class DummyConfig(BaseConfig):
            def load(self):
                self._variables["test1"] = True
                self.is_loaded = True
                return True

        c = DummyConfig("nonexistent.txt")
        self.assertTrue(c.read())

    def test_repr(self):
        class DummyConfig(BaseConfig):
            def load(self):
                self._variables["test1"] = True

        c = DummyConfig("nonexistent.txt")
        c.load()
        self.assertEqual("<DummyConfig nonexistent>", repr(c))


class TestFlatConfig(unittest.TestCase):

    def test_load(self):
        c = FlatConfig("nonexistent.cfg")
        self.assertFalse(c.load())
        self.assertFalse(c.is_loaded)

        c = FlatConfig(EXAMPLE_CFG)
        self.assertTrue(c.load())
        self.assertTrue(c.is_loaded)

        self.assertEqual(c.project_title, "Example Project")

    def test_load_with_context(self):
        context = {
            'client_code': "ACME",
            'client_name': "ACME, Inc."
        }
        c = FlatConfig(EXAMPLE_CFG_TEMPLATE, context=context)
        self.assertTrue(c.load())
        self.assertTrue(c.is_loaded)

        self.assertEqual(c.client_code, "ACME")


class TestINIConfig(unittest.TestCase):

    def test_getattr(self):
        c = INIConfig(EXAMPLE_INI)
        c.load()

        self.assertEqual(c.project.title, "Example Project")

    def test_get_sections(self):
        c = INIConfig(EXAMPLE_INI)
        c.load()

        for s in c.get_sections():
            self.assertIsInstance(s, Section)

    def test_has(self):
        c = INIConfig(EXAMPLE_INI)
        c.load()

        self.assertTrue(c.has("project"))
        self.assertTrue(c.has("project", key="title"))
        self.assertFalse(c.has("business"))
        self.assertFalse(c.has("business", key="name"))

    def test_iter(self):
        c = INIConfig(EXAMPLE_INI)
        c.load()

        self.assertTrue("project" in c)

    def test_load(self):
        c = INIConfig("nonexistent.ini")
        self.assertFalse(c.load())
        self.assertFalse(c.is_loaded)

        c = INIConfig(EXAMPLE_INI)
        self.assertTrue(c.load())
        self.assertTrue(c.is_loaded)

    def test_load_defaults(self):
        class TestConfig(INIConfig):
            def _get_default_sections(self):
                return [
                    "project",
                    "client",
                ]

            def _get_default_values(self):
                return [
                    ("client", "code", "UNK"),
                    ("client", "name", "Unknown"),
                    ("project", "active", True),
                    ("project", "title", "Untitled Project"),
                    ("project", "release", 1),
                ]

        c = TestConfig("nonexistent.ini")
        self.assertFalse(c.load())
        self.assertFalse(c.is_loaded)

        self.assertEqual(c.project.title, "Untitled Project")

    def test_load_with_context(self):
        context = {
            'client_code': "ACME",
            'client_name': "ACME, Inc."
        }
        c = INIConfig(EXAMPLE_INI_TEMPLATE, context=context)
        self.assertTrue(c.load())
        self.assertTrue(c.is_loaded)

        self.assertEqual(c.client.code, "ACME")


class TestPYConfig(unittest.TestCase):

    def test_getattr(self):
        c = PYConfig(EXAMPLE_PY)
        c.load()

        self.assertEqual(c.test1, "abc")

    def test_has(self):
        c = PYConfig(EXAMPLE_PY)
        c.load()

        self.assertTrue(c.has("test1"))
        self.assertFalse(c.has("test4"))

    def test_load(self):
        c = PYConfig("nonexistent.py")
        self.assertFalse(c.load())
        self.assertFalse(c.is_loaded)

        c = PYConfig(EXAMPLE_PY)
        self.assertTrue(c.load())
        self.assertTrue(c.is_loaded)


class TestFactory(unittest.TestCase):

    def test_factory(self):
        c = factory(EXAMPLE_CFG)
        self.assertIsInstance(c, FlatConfig)

        c = factory(EXAMPLE_INI)
        self.assertIsInstance(c, INIConfig)

        c = factory(EXAMPLE_PY)
        self.assertIsInstance(c, PYConfig)

        self.assertRaises(RuntimeError, factory, "example.xml")
