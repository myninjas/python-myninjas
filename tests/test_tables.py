from myninjas.context_managers import captured_output
from myninjas.tables.library import *
import unittest

# Tests


class TestTable(unittest.TestCase):

    def test_add(self):
        headings = [
            "Column 1",
            "Column 2",
            "Column 3",
        ]

        table = Table(headings=headings)
        table.add(["a", "b", "c"])
        table.add(["d", "e", "f"])
        table.add(["g", "h", "i"])

        self.assertEqual(3, len(table._rows))
        self.assertEqual(2, table._rows[1].number)

    def test_iter(self):
        headings = [
            "Column 1",
            "Column 2",
            "Column 3",
        ]

        table = Table(headings=headings)
        table.add(["a", "b", "c"])
        table.add(["d", "e", "f"])
        table.add(["g", "h", "i"])

        for row in table:
            self.assertIsInstance(row, Row)

            for value in row:
                self.assertIsInstance(value, str)

    def test_str(self):
        headings = [
            "Column 1",
            "Column 2",
            "Column 3",
        ]

        table = Table(headings=headings)
        table.add(["a", "b", "c"])
        table.add(["d", "e", "f"])
        table.add(["g", "h", "i"])

        with captured_output() as (output, error):
            print(table)
            self.assertTrue("Column 1" in output.getvalue())
            self.assertTrue("---" in output.getvalue())

    def test_to_string(self):
        headings = [
            "Column 1",
            "Column 2",
            "Column 3",
        ]

        table = Table(headings=headings)
        table.add(["a", "b", "c"])
        table.add(["d", "e", "f"])
        table.add(["g", "h", "i"])

        output = table.to_string()
        self.assertTrue("Column 1" in output)
        self.assertTrue("---" in output)
