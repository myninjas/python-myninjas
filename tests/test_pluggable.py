from myninjas.pluggable import PluginManager
import unittest


class TestPluginManager(unittest.TestCase):
    """Testing the plugin manager is sufficient for testing related resources."""

    def test_load(self):
        enabled_plugins = [
            "tests.plugins.testing1",
            "tests.plugins.testing2",
            "tests.plugins.testing3",
            "tests.plugins.nonexistent",
        ]

        manager = PluginManager(plugins=enabled_plugins)
        count = manager.load()

        # Three plugins exist.
        count_3 = 3
        self.assertEqual(count_3, count)

        # One plugin does not exist.
        count_1 = 1
        self.assertEqual(count_1, len(manager.errors))

        # Plugs will not be loaded a second time.
        count = manager.load()
        self.assertEqual(count_3, count)

        # Iterating over the plugins should produce the same count.
        count = 0
        # noinspection PyUnusedLocal
        for plugin in manager:
            count += 1

        self.assertEqual(count_3, count)
