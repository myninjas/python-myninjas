from myninjas.dispatcher import Signal
import unittest


# Example Functions


# noinspection PyUnusedLocal
def example_exception_receiver(**kwargs):
    raise NotImplementedError("This receiver always raises an exception.")


# noinspection PyUnusedLocal
def example_fail_receiver(**kwargs):
    return False, "This receiver always fails."


# noinspection PyUnusedLocal
def example_success_receiver(**kwargs):
    return True, "This receiver is always successful."

# Example Classes


class ExampleSender(object):
    pass

# Tests


class TestSignal(unittest.TestCase):

    def test_connect(self):
        """Check that signals may be connected under various conditions."""
        my_signal = Signal(arguments=["testing"])

        # No uid or sender.
        my_signal.connect(example_success_receiver)
        count_1 = 1
        self.assertEqual(count_1, len(my_signal.receivers))

        # Might as well check the identifier.
        self.assertIsNotNone(my_signal.receivers[0].identifier)
        self.assertIsNotNone(my_signal.receivers[0].callback_id)
        self.assertIsNotNone(my_signal.receivers[0].sender_id)

        # With uid.
        my_signal.connect(example_fail_receiver, dispatch_uid="my_example_fail_receiver")
        count_2 = 2
        self.assertEqual(count_2, len(my_signal.receivers))

        # With sender.
        my_signal.connect(example_exception_receiver, sender=ExampleSender)
        count_3 = 3
        self.assertEqual(count_3, len(my_signal.receivers))

        # Duplicate.
        my_signal.connect(example_success_receiver)
        self.assertEqual(count_3, len(my_signal.receivers))

        # Duplicate manually uid.
        my_signal.connect(example_fail_receiver, dispatch_uid="my_example_fail_receiver")
        self.assertEqual(count_3, len(my_signal.receivers))

    def test_disconnect(self):
        """Check that signal receivers may be disconnected."""
        my_signal = Signal(arguments=["testing"])

        count_0 = 0

        # Without a sender.
        my_signal.connect(example_success_receiver)
        my_signal.disconnect(example_success_receiver)

        self.assertEqual(count_0, len(my_signal.receivers))

        # With sender.
        my_signal.connect(example_success_receiver, sender=ExampleSender)
        my_signal.disconnect(example_success_receiver, sender=ExampleSender)
        self.assertEqual(count_0, len(my_signal.receivers))

        # With sender and dispatch UID.
        my_signal.connect(example_success_receiver, dispatch_uid="testing_signal_receiver", sender=ExampleSender)
        my_signal.disconnect(example_success_receiver, dispatch_uid="testing_signal_receiver", sender=ExampleSender)
        self.assertEqual(count_0, len(my_signal.receivers))

    def test_get_callbacks(self):
        """Check that getting callbacks work as expected."""
        my_signal = Signal(arguments=["testing"])

        # No sender so should respond to all signals.
        my_signal.connect(example_success_receiver)
        count_1 = 1
        self.assertEqual(count_1, len(my_signal.get_callbacks(ExampleSender)))

        # With sender.
        my_signal.connect(example_fail_receiver, sender=ExampleSender)
        count_2 = 2
        self.assertEqual(count_2, len(my_signal.get_callbacks(ExampleSender)))

    def test_send(self):
        """Check that signals send as expected."""
        my_signal = Signal(arguments=["testing"])

        # No responses because nothing registered.
        count_0 = 0
        responses = my_signal.send(ExampleSender, testing=True)
        self.assertEqual(count_0, len(responses))

        # Successful response.
        my_signal.connect(example_success_receiver, sender=ExampleSender)
        responses = my_signal.send(ExampleSender, testing=True)
        count_1 = 1
        self.assertEqual(count_1, len(responses))

        my_signal.disconnect(example_success_receiver, sender=ExampleSender)

        # Unsuccessful response.
        my_signal.connect(example_fail_receiver, sender=ExampleSender)
        responses = my_signal.send(ExampleSender, testing=True)
        count_1 = 1
        self.assertEqual(count_1, len(responses))

        my_signal.disconnect(example_fail_receiver, sender=ExampleSender)

        # Exception response.
        my_signal.connect(example_exception_receiver, sender=ExampleSender)
        responses = my_signal.send(ExampleSender, testing=True)
        count_1 = 1
        self.assertEqual(count_1, len(responses))

        self.assertIsInstance(responses[0].error, NotImplementedError)
