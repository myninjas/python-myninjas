from myninjas.context_managers import captured_output
from myninjas.feedback.library import *
import unittest

# Tests


class TestFeedback(unittest.TestCase):

    def test_cr(self):
        feedback = Feedback()
        feedback.plain("This is a message.")
        feedback.cr()
        self.assertEqual(2, len(feedback.messages))

    def test_feedback(self):
        """Check standard feedback output."""
        feedback = Feedback()
        feedback.blue("This is an INFORMATIONAL message.")
        feedback.plain("This is a just a message message.")
        feedback.green("This is a SUCCESS message.")
        feedback.red("This is an ERROR message.")
        feedback.yellow("This is a WARNING message.")
        feedback.hr()

        self.assertEqual(6, len(feedback.messages))

    def test_heading(self):
        feedback = Feedback()
        feedback.heading("Testing")
        feedback.plain("This is a message.")
        self.assertEqual(4, len(feedback.messages))

    def test_hr(self):
        """Check that horizontal rules are generated properly."""
        feedback = Feedback()
        feedback.hr(character="=", color=red, size=10)

        # Additional characters are added by the colorization.
        self.assertEqual(20, len(feedback.messages[0]))

    def test_iter(self):
        """Check that message instances are returned."""
        feedback = Feedback()
        feedback.blue("This is an INFORMATIONAL message.")
        feedback.plain("This is a just a message message.")
        feedback.green("This is a SUCCESS message.")
        feedback.red("This is an ERROR message.")
        feedback.yellow("This is a WARNING message.")
        feedback.hr()

        for message in feedback:
            self.assertIsInstance(message, str)

    def test_plain(self):
        feedback = Feedback()
        feedback.plain("This is a message.", prefix="Prefix:", suffix="wow")
        self.assertEqual("Prefix: This is a message. wow", feedback.messages[0])

    def test_str(self):
        feedback = Feedback()
        feedback.blue("This is an INFORMATIONAL message.")
        feedback.plain("This is a just a message message.")
        feedback.hr()

        with captured_output() as (output, error):
            print(feedback)
            self.assertTrue("INFORMATIONAL" in output.getvalue())
            self.assertTrue("just a message" in output.getvalue())
            self.assertTrue("---" in output.getvalue())


class TestFunctions(unittest.TestCase):

    def test_blue(self):
        with captured_output() as (output, error):
            blue("This is blue.")

            self.assertTrue("This is blue." in output.getvalue())

    def test_colorize(self):
        """Check output of colorize function."""
        from colorama import Fore

        output = colorize(Fore.RED, "This is a test", prefix="Testing:", suffix="<-- look")
        self.assertTrue("This is a test" in output)
        self.assertTrue("Testing:" in output)
        self.assertTrue("<-- look" in output)

    def test_green(self):
        with captured_output() as (output, error):
            green("This is green.")
            self.assertTrue("This is green." in output.getvalue())

    def test_hr(self):
        with captured_output() as (output, error):
            hr(character="=", color=red, size=10)
            self.assertTrue("===" in output.getvalue())

        with captured_output() as (output, error):
            hr(character="=", size=10)
            self.assertTrue("===" in output.getvalue())

    def test_plain(self):
        with captured_output() as (output, error):
            plain("This is plain.", prefix="Testing:", suffix="wow")
            self.assertTrue("This is plain." in output.getvalue())

    def test_red(self):
        with captured_output() as (output, error):
            red("This is red.")
            self.assertTrue("This is red." in output.getvalue())

    def test_yellow(self):
        with captured_output() as (output, error):
            yellow("This is yellow.")
            self.assertTrue("This is yellow." in output.getvalue())
