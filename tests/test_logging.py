import logging
from myninjas.logging import BaseFormatter, LoggingHelper, SuccessLogger
import os
from testfixtures import LogCapture
import unittest

# See https://www.pythonhosted.org/testfixtures/logging.html for better testing?

# Tests


class TestBaseFormatter(unittest.TestCase):
    """Pick up coverage for things no directly tested by TestLoggingHelper."""

    def test_get_level_name(self):
        """Check that the level name returns the same name."""
        formatter = BaseFormatter()
        name = formatter._get_level_name("INFO")
        self.assertEqual("INFO", name)


class TestLoggingHelper(unittest.TestCase):

    def setUp(self):
        self.log_path = os.path.join("tests", "tmp.log")

    def tearDown(self):
        if os.path.exists(self.log_path):
            os.remove(self.log_path)

    def test_color_enabled(self):
        pass

    def test_get_console_formatter(self):
        pass

    def test_get_file_formatter(self):
        pass

    def test_setup_default(self):
        """Check the default friendly setup."""
        log = LoggingHelper()
        logger = log.setup()

        # noinspection PyUnusedLocal
        with LogCapture() as capture:
            logger.debug("test_setup_default() This is a debug message.")
            logger.info("test_setup_default() This message is informational.")
            logger.warning("test_setup_default() You better watch out!")
            logger.critical("test_setup_default() Something even worse has happened.")

        # This forces a call to formatException(). However, if fails if used with LogCapture.
        logger.error("You can ignore this error.", exc_info=True)

    def test_setup_plain(self):
        """Check plain text friendly setup."""

        log = LoggingHelper(colorize=False)
        log.setup()

        logger = logging.getLogger()

        # Using LogCapture suppresses coverage.
        logger.info("test_setup_plain() This message is informational.")

        # noinspection PyUnusedLocal
        with LogCapture() as capture:
            logger.debug("test_setup_plain() This is a debug message.")
            logger.info("test_setup_plain() This message is informational.")
            logger.warning("test_setup_plain() You better watch out!")
            logger.error("test_setup_plain() Something bad has happened.")
            logger.critical("test_setup_plain() Something even worse has happened.")

    def test_setup_path(self):
        """Check friendly setup for log file."""

        log = LoggingHelper(colorize=False, console=False, path=self.log_path)
        log.setup()

        logger = logging.getLogger()

        # Using LogCapture suppresses coverage.
        logger.debug("test_setup_path() This is a debug message.")
        logger.info("test_setup_path() This message is informational.")
        logger.warning("test_setup_path() You better watch out!")
        logger.error("test_setup_path() Something bad has happened.")
        logger.critical("test_setup_path() Something even worse has happened.")


class TestSuccessLogger(unittest.TestCase):

    def test_success(self):
        log = LoggingHelper(success=True, name="success-logger")
        logger = log.setup()

        # Using LogCapture suppresses coverage.
        logger.success("test_success() This is a success message.")
