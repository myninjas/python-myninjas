from myninjas.context_managers import captured_output
from myninjas.prompt import *
import unittest
# noinspection PyCompatibility
from unittest.mock import patch


class TestingForm(Form):
    input1 = String("input1")
    input2 = String("input2")
    input3 = String("input3")


# Tests


class TestFunctions(unittest.TestCase):

    def test_get_choice(self):
        # Without choices.
        with patch('builtins.input', return_value="1"):
            with captured_output() as (output, error):
                result = get_choice("Yes or No")
                self.assertEqual("yes", result)

        # With choices.
        choices = ["a", "b", "c"]
        with patch('builtins.input', return_value="3"):
            with captured_output() as (output, error):
                result = get_choice("Pick a Letter", choices=choices)
                self.assertEqual("c", result)

        # With default.
        choices = ["a", "b", "c"]
        with patch('builtins.input', return_value=""):
            with captured_output() as (output, error):
                result = get_choice("Pick a Letter", choices=choices, default="b")
                self.assertEqual("b", result)

        # With invalid choice.
        try:
            choices = ["a", "b", "c"]
            with patch('builtins.input', return_value="4"):
                with captured_output() as (output, error):
                    result = get_choice("Pick a Letter", choices=choices, default="b")
                    # self.assertTrue("Invalid" in output.getstring())
        except RecursionError:
            pass

    def test_get_input(self):
        # Without default value.
        with patch('builtins.input', return_value="testing"):
            with captured_output() as (output, error):
                result = get_input("What Is This?")
                self.assertEqual("testing", result)

        # With default value.
        with patch('builtins.input', return_value=""):
            with captured_output() as (output, error):
                result = get_input("What Is This", default="testing")
                self.assertEqual("testing", result)


class TestForm(unittest.TestCase):

    def test_default(self):
        form = TestingForm()
        form.fields['input2'].default = "$input1"

        with patch('builtins.input', return_value="testing"):
            with captured_output() as (output, error):
                form.prompt()
                self.assertTrue(len(form.values) == 3)

    def test_get(self):
        form = TestingForm()
        value = form.get("input1", default="testing")
        self.assertEqual("testing", value)

    def test_get_fields(self):
        form = TestingForm()
        self.assertTrue(len(form.get_fields()) == 3)

    def test_prompt(self):
        form = TestingForm()

        with patch('builtins.input', return_value="testing"):
            with captured_output() as (output, error):
                form.prompt()
                self.assertTrue(len(form.values) == 3)


class TestInput(unittest.TestCase):

    def test_get_type(self):
        i = Input("testing")
        self.assertEqual(str, i.get_type())

    def test_is_valid(self):
        i = Input("testing")
        self.assertTrue(i.is_valid())

        i = Input("Testing", required=True)
        self.assertFalse(i.is_valid())
        self.assertEqual("Testing is required.", i.error)

    def test_prompt(self):
        i = Input("testing")

        with patch('builtins.input', return_value="yes"):
            with captured_output() as (output, error):
                result = i.prompt()
                self.assertEqual("yes", result)

        choices = ["a", "b", "c"]
        i = Input("Pick a Letter", choices=choices, required=True)
        with patch('builtins.input', return_value="3"):
            with captured_output() as (output, error):
                result = i.prompt()
                self.assertEqual("c", result)

        try:
            i = Input("testing", required=True)
            with patch('builtins.input', return_value=""):
                with captured_output() as (output, error):
                    i.prompt()
                    # self.assertTrue("required" in output.getstring())
        except RecursionError:
            pass

    def test_repr(self):
        i = Input("testing")
        self.assertEqual("<Input testing>", repr(i))


class TestBoolean(unittest.TestCase):

    def test_get_type(self):
        i = Boolean("testing")
        self.assertEqual(bool, i.get_type())

    def test_is_valid(self):
        i = Boolean("testing", required=True)
        self.assertFalse(i.is_valid())

        i.value = "not-a-boolean"
        self.assertFalse(i.is_valid())

        i.value = "no"
        self.assertTrue(i.is_valid())

        i.value = "yes"
        self.assertTrue(i.is_valid())
        self.assertEqual(True, i.to_python())


class TestDivider(unittest.TestCase):

    def test_prompt(self):
        i = Divider(label="Testing")
        with captured_output() as (output, error):
            i.prompt()

            self.assertTrue("Testing" in output.getvalue())
            self.assertTrue("===" in output.getvalue())


class TestEmail(unittest.TestCase):

    def test_is_valid(self):
        i = Email("testing", required=True)
        self.assertFalse(i.is_valid())

        i.value = "invalidemail.com"
        self.assertFalse(i.is_valid())

        i.value = "example@example.com"
        self.assertTrue(i.is_valid())


class TestFloat(unittest.TestCase):

    def test_get_type(self):
        i = Float("testing")
        self.assertEqual(float, i.get_type())

    def test_is_valid(self):
        i = Float("testing", required=True)
        self.assertFalse(i.is_valid())

        i.value = "not-a-float"
        self.assertFalse(i.is_valid())

        i.value = "1.2345"
        self.assertTrue(i.is_valid())

        i.value = 1.2345
        self.assertTrue(i.is_valid())


class TestInteger(unittest.TestCase):

    def test_get_type(self):
        i = Integer("testing")
        self.assertEqual(int, i.get_type())

    def test_is_valid(self):
        i = Integer("testing", required=True)
        self.assertFalse(i.is_valid())

        i.value = "not-an-integer"
        self.assertFalse(i.is_valid())

        i.value = "1.2"
        self.assertFalse(i.is_valid())

        i.value = 1
        self.assertTrue(i.is_valid())


class TestRegex(unittest.TestCase):

    def test_is_valid(self):
        pattern = r"[0-9]*"
        i = Regex("testing", pattern, required=True)
        self.assertFalse(i.is_valid())

        i.value = "this doesn't match"
        self.assertFalse(i.is_valid())

        i.value = "1234"
        self.assertTrue(i.is_valid())

    def test_to_python(self):
        pattern = r"[0-9]*"
        i = Regex("testing", pattern, required=True)
        i.value = "this doesn't match"
        self.assertIsNone(i.to_python())


class TestSecret(unittest.TestCase):

    def test_prompt(self):
        i = Secret("password")

        with patch("getpass._raw_input", return_value="secret"):
            with captured_output() as (output, error):
                result = i.prompt()
                self.assertEqual("secret", result)

        i.required = True
        try:
            with patch('getpass._raw_input', return_value=""):
                with captured_output() as (output, error):
                    i.prompt()
        except RecursionError:
            pass


class TestString(unittest.TestCase):

    def test_is_valid(self):
        i = String("testing", maximum=10, minimum=3)
        self.assertFalse(i.is_valid())

        i.value = "12"
        self.assertFalse(i.is_valid())

        i.value = "12345678910"
        self.assertFalse(i.is_valid())

        i.value = "123"
        self.assertTrue(i.is_valid())

        i.value = "123456789"
        self.assertTrue(i.is_valid())
