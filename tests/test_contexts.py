import os
from myninjas.contexts.managers import captured_output, cd, virtualenv
from myninjas.contexts import Context
from myninjas.shell import Command
import unittest
# noinspection PyCompatibility
from unittest.mock import patch


class TestContext(unittest.TestCase):

    def test_add(self):
        c = Context("testing")

        c.add("test1", "abc")
        self.assertTrue(c.has("test1"))
        self.assertEqual(c.get("test1"), "abc")

        self.assertRaises(ValueError, c.add, "test1", "def")

    def test_get(self):
        c = Context("testing")

        c.add("test1", "abc")
        self.assertTrue(c.has("test1"))
        self.assertEqual(c.get("test1"), "abc")

        self.assertEqual(c.get("test2", default="def"), "def")

    def test_get_name(self):
        c = Context("testing")

        self.assertEqual(c.get_name(), "testing")

    def test_getattr(self):
        c = Context("testing")

        c.add("test1", "abc")
        c.add("test2", 123)

        self.assertEqual(c.test1, "abc")
        self.assertEqual(c.test2, 123)

    def test_has(self):
        c = Context("testing")

        c.add("test1", "abc")
        c.add("test2", 123)
        c.add("test3", None)

        self.assertTrue(c.has("test1"))
        self.assertTrue(c.has("test2"))
        self.assertFalse(c.has("test3"))

    def test_init(self):
        defaults = {
            'test1': "abc",
            'test2': 123,
        }
        c = Context("testing", defaults=defaults)

        self.assertTrue(c.has("test1"))
        self.assertTrue(c.has("test2"))
        self.assertFalse(c.has("test3"))

    def test_iter(self):
        c = Context("testing")

        c.add("test1", "abc")
        c.add("test2", 123)
        c.add("test3", None)

        for i in c:
            self.assertIsInstance(i, str)

    def test_mapping(self):
        c = Context("testing")

        c.add("test1", "abc")
        c.add("test2", 123)
        c.add("test3", None)

        self.assertTrue(len(c.mapping()) == 3)
        self.assertTrue(type(c.mapping()) == dict)

    def test_repr(self):
        c = Context("testing")

        self.assertEqual("<Context testing>", repr(c))

    def test_set(self):
        c = Context("testing")
        c.set("test1", "abc")
        self.assertTrue("abc", c.test1)

    def test_update(self):
        c = Context("testing")

        c.add("test1", "abc")
        c.add("test2", 123)
        c.add("test3", None)

        c.update("test3", "def")
        self.assertTrue(c.test3 == "def")

        self.assertRaises(KeyError, c.update, "test4", 456)


class TestContextManagers(unittest.TestCase):

    def test_captured_output(self):
        """Check that captured output works as expected."""
        with patch('builtins.input', return_value='yes'):
            with captured_output() as (output, error):
                result = input("This is a Test (yes/no)? ")
                self.assertEqual("yes", result)

    def test_cd(self):
        """Check that cd works as expected."""
        path = os.path.join("tests", "data")
        with cd(path):
            command = Command("ls -ls")
            command.run()
            self.assertTrue("example.csv" in command.output)

    def test_virtualenv(self):
        """Check that virtualenv works as expected."""
        with virtualenv("./manage.py migrate") as command:
            self.assertEqual("source python/bin/activate && ./manage.py migrate", command)
