import os
import shutil
from myninjas.watchers import Watcher
import time
import unittest

# Tests


class TestWatch(unittest.TestCase):

    def setUp(self):
        self.output_path = os.path.join("tests", "watched")
        # if not os.path.exists(self.output_path):
        #     os.makedirs(self.output_path)

    def test_exists(self):
        """Check that a watcher correctly identifies its existence."""
        watcher = Watcher(self.output_path)
        self.assertTrue(watcher.exists)

        nonexistent_path = os.path.join(self.output_path, "nonexistent.txt")
        watcher = Watcher(nonexistent_path)

        self.assertFalse(watcher.exists)

    def test_is_directory(self):
        """Check that a watcher correctly identifies as a directory."""
        watcher = Watcher(self.output_path)

        self.assertTrue(watcher.is_directory)
        self.assertFalse(watcher.is_file)

        example_path = os.path.join("tests", "watched", "readme.txt")
        watcher = Watcher(example_path)

        self.assertTrue(watcher.is_file)
        self.assertFalse(watcher.is_directory)

    def test_watch(self):

        # This watcher has an invalid path.
        watcher = Watcher(os.path.join("path", "to", "nonexistent"))
        try:
            next(watcher.watch())
        except ValueError:
            self.assertTrue(True)

        # Watcher for the directory.
        directory_watcher = Watcher(self.output_path)

        # Watcher for a specific file.
        file_path = os.path.join(self.output_path, "readme.txt")
        file_watcher = Watcher(file_path)

        # First pass returns True.
        self.assertTrue(next(directory_watcher.watch()))
        self.assertTrue(next(file_watcher.watch()))

        # The next pass should return False because nothing's been modified.
        self.assertFalse(next(directory_watcher.watch()))
        self.assertFalse(next(file_watcher.watch()))

        # Update time to detect modification.
        t = time.time()
        os.utime(file_path, (t, t))

        self.assertTrue(next(directory_watcher.watch()))
        self.assertTrue(next(file_watcher.watch()))

        # A bad file produces an OSError and returns None.
        bad_file_watcher = Watcher("path/to/nonexistent.txt")
        bad_file_watcher._is_file = True
        self.assertIsNone(next(bad_file_watcher.watch()))

        # The two nonexistent files (symlinks to nowhere) will result in a yield of None, even when .md extension is
        # included. This helps force an OSError.
        specific_watcher = Watcher(self.output_path, extensions=[".md"])
        self.assertIsNone(next(specific_watcher.watch()))

        # Check functionality against an initially empty path with added files and extension filtering.
        empty_path = os.path.join(self.output_path, "empty")

        os.mkdir(empty_path)

        # An empty directory should return None.
        empty_watcher = Watcher(empty_path)
        self.assertIsNone(next(empty_watcher.watch()))

        # Create a file that isn't watched.
        from_path = os.path.join(self.output_path, "readme.txt")
        to_path = os.path.join(empty_path, "readme.txt")
        shutil.copy(from_path, to_path)

        # .txt extension should return None.
        specific_watcher = Watcher(empty_path, extensions=[".md"])
        self.assertIsNone(next(specific_watcher.watch()))

        # No create a file that is watched.
        to_path = os.path.join(empty_path, "readme.md")
        shutil.copy(from_path, to_path)
        self.assertTrue(next(specific_watcher.watch()))

        # Remove the empty_path to reset for the future.
        shutil.rmtree(empty_path, ignore_errors=True)
