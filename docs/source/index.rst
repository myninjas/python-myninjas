My Ninjas (Python)
==================

Contents:

.. toctree::
    :maxdepth: 3

    Reference <reference>
    Developer <developer>
    Tests <tests>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
