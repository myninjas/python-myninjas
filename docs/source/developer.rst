.. _developer-reference:

*********
Developer
*********

Dependencies
============

Colorama
--------

The `Colorama package`_ is used for colorizing command line output.

.. _Colorama package: https://pypi.python.org/pypi/colorama

Jinja2
------

Used for template rendering.

Sphinx
------

Sphinx is used for documentation along with ``sphinx_rtd_theme`` and `sphinx-helpers`_.

.. _sphinx-helpers: https://github.com/develmaycare/sphinx-helpers

Library
=======

contexts
--------

.. automodule:: myninjas.contexts.library
    :members:
    :show-inheritance:
    :special-members: __init__

.. automodule:: myninjas.contexts.managers
    :members:
    :show-inheritance:
    :special-members: __init__

context_managers
----------------

.. warning::
    Use of the ``context_managers`` library is deprecated. Use ``contexts.managers`` instead.

.. automodule:: myninjas.context_managers.library
    :members:
    :show-inheritance:
    :special-members: __init__

dispatcher
----------

.. automodule:: myninjas.dispatcher.library
    :members:
    :show-inheritance:
    :special-members: __init__

feedback
--------

.. automodule:: myninjas.feedback.library
    :members:
    :show-inheritance:
    :special-members: __init__

logging
-------

.. automodule:: myninjas.logging.library
    :members:
    :show-inheritance:
    :special-members: __init__

platform
--------

.. automodule:: myninjas.platform.library
    :members:
    :show-inheritance:
    :special-members: __init__

pluggable
---------

.. automodule:: myninjas.pluggable.library
    :members:
    :show-inheritance:
    :special-members: __init__

prompt
------

.. automodule:: myninjas.prompt.library
    :members:
    :show-inheritance:
    :special-members: __init__


settings
--------

.. automodule:: myninjas.settings.library
    :members:
    :show-inheritance:
    :special-members: __init__

shell
-----

.. automodule:: myninjas.shell.library
    :members:
    :show-inheritance:
    :special-members: __init__

.. automodule:: myninjas.shell.library.apache
    :members:
    :show-inheritance:
    :special-members: __init__

.. automodule:: myninjas.shell.library.django
    :members:
    :show-inheritance:
    :special-members: __init__

.. automodule:: myninjas.shell.library.messages
    :members:
    :show-inheritance:
    :special-members: __init__

.. automodule:: myninjas.shell.library.packaging
    :members:
    :show-inheritance:
    :special-members: __init__

.. automodule:: myninjas.shell.library.pgsql
    :members:
    :show-inheritance:
    :special-members: __init__

.. automodule:: myninjas.shell.library.services
    :members:
    :show-inheritance:
    :special-members: __init__

.. automodule:: myninjas.shell.library.ssl
    :members:
    :show-inheritance:
    :special-members: __init__

.. automodule:: myninjas.shell.library.stackscripts
    :members:
    :show-inheritance:
    :special-members: __init__

.. automodule:: myninjas.shell.library.system
    :members:
    :show-inheritance:
    :special-members: __init__

.. automodule:: myninjas.shell.library.tarball
    :members:
    :show-inheritance:
    :special-members: __init__

.. automodule:: myninjas.shell.library.templates
    :members:
    :show-inheritance:
    :special-members: __init__

tables
------

.. automodule:: myninjas.tables.library
    :members:
    :show-inheritance:
    :special-members: __init__

utils
-----

.. automodule:: myninjas.utils.library
    :members:
    :show-inheritance:
    :special-members: __init__

watchers
--------

.. automodule:: myninjas.watchers.library
    :members:
    :show-inheritance:
    :special-members: __init__
