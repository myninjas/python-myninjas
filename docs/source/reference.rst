*********
Reference
*********

Contexts
========

.. automodule:: myninjas.contexts

Context Managers
================

.. warning::
    Use of the ``context_managers`` library is deprecated. Use ``contexts.managers`` instead.

.. automodule:: myninjas.context_managers

Dispatcher and Signals
======================

.. automodule:: myninjas.dispatcher

Feedback
========

.. automodule:: myninjas.feedback

Logging
=======

.. automodule:: myninjas.logging

Platform
========

.. automodule:: myninjas.platform

Plugin System
=============

.. automodule:: myninjas.pluggable

Prompt
======

.. automodule:: myninjas.prompt

Settings
========

.. automodule:: myninjas.settings

Shell Commands
==============

.. automodule:: myninjas.shell

Tables
======

.. automodule:: myninjas.tables

Utilities
=========

.. automodule:: myninjas.utils

Watchers
========

.. automodule:: myninjas.watchers
