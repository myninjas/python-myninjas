# My Ninjas (Python)

![](https://img.shields.io/badge/status-active-green.svg)
![](https://img.shields.io/badge/stage-alpha-orange.svg)
![](https://img.shields.io/badge/coverage-100%25-green.svg)

A collection of libraries and utilities that may be useful for most Python projects.

## Requirements

- Python 3
- See also the dependencies in ``requirements.pip``.

## Installation

> Note: Installation using ``pip`` is coming with the first official release.

From source code:

```bash
pip install git+https://bitbucket.org/myninjas/python-myninjas/master.tar.gz;
```

## Documentation

[Detailed documentation available](https://myninjas.net/docs/python-myninjas/).

## Tests

100% code coverage in the master branch. See [coverage report](https://myninas.net/docs/python-ninjas/coverage/).

## License

Copyright Pleasant Tents, LLC

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
   disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

---

![](assets/myninjas-logo.jpg)
