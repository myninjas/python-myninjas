#! /usr/bin/env python

from collections import OrderedDict
import inspect
from myninjas.shell.library.mappings import MAPPING


# https://stackoverflow.com/a/52003056/241720
def get_signature(fn):
    params = inspect.signature(fn).parameters
    args = []
    kwargs = OrderedDict()
    for p in params.values():
        if p.default is p.empty:
            args.append(p.name)
        else:
            kwargs[p.name] = p.default
    return args, kwargs


keys = list(MAPPING.keys())
keys.sort()

for key in keys:
    cls = MAPPING[key]

    print(key)
    print("^" * len(key))
    print("")

    print(".. code-block:: cfg")
    print("")

    if cls.__doc__:
        print("    %s" % cls.__doc__.strip().replace(".", ":").lower())
    else:
        print("    run a %s command:" % cls.__name__.lower())

    args, kwargs = get_signature(cls.__init__)

    line = list()
    for a in args:
        if a not in ("self", "kwargs"):
            line.append(a)

    print("        %s %s" % (key, " ".join(line)))

    for option, value in kwargs.items():
        print("        %s %s" % (option, value))

    print("")

'''
for key in keys:
    cls = MAPPING[key]
    signature = inspect.signature(cls.__init__)

    # print("**%s**" % key)
    print(key)
    print("^" * len(key))
    print("")
    print(".. code-block:: cfg")
    print("")

    if cls.__doc__:
        print("    %s" % cls.__doc__.strip().replace(".", ":").lower())

    if key in ("apache.config", "apache.configtest", "apache.test"):
        print("        %s" % key)
        print("")
        continue

    # if cls.__init__.__doc__:
    #     lines = cls.__init__.__doc__.split("\n")
    #     for line in lines:
    #         if ":param" in line:

    # print(cls.__init__.__doc__)
    # print("")

    a = list()
    index = 0
    for name, value in signature.parameters.items():
        if name == "self":
            continue

        if name == "kwargs":
            name = "(options)"
            continue

        if index == 0:
            a.append("        %s %s" % (key, name))
            index += 1
            continue

        index += 1

        if value.default is True:
            name = "%s yes" % name
        elif value.default is False:
            name = "%s no" % name
        elif value.default is None:
            name = "%s None" % name
        elif type(value.default) is int:
            name = "%s %s" % (name, value.default)
        elif type(value.default) is str:
            name = "%s %s" % (name, value.default)
        else:
            pass

        a.append("        %s" % name)

    print("\n".join(a))
    print("")
'''
